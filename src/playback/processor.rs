use cpal;
use playback::streams::{AudioConsumer, SharedStreamMap};
use playback::{init_backend, BackendLoop};
use rb::RbConsumer;
use std::sync::Arc;
use std::thread;

pub struct AudioProcessor {
    backend_loop: BackendLoop,
    device: cpal::Device,
    formats: Vec<cpal::SupportedFormat>,
}

impl AudioProcessor {
    pub fn create() -> AudioProcessor {
        let device = cpal::default_output_device().expect("Failed to get default audio output device");
        info!("Using audio device: {:?}", device.name());
        debug!("Enumerating device capabilities...");
        let formats: Vec<cpal::SupportedFormat> = device
            .supported_output_formats()
            .expect("Failed to enumerate device capabilities")
            .collect();

        AudioProcessor {
            backend_loop: init_backend(),
            formats: formats,
            device: device,
        }
    }

    pub fn device(&self) -> &cpal::Device {
        &self.device
    }

    pub fn default_sample_format(&self) -> cpal::SampleFormat {
        self.device
            .default_output_format()
            .expect("Failed to query default output format")
            .data_type
    }

    pub fn default_sample_rate(&self) -> u32 {
        self.device
            .default_output_format()
            .expect("Failed to query default output format")
            .sample_rate
            .0
    }

    pub fn play_stream(&self, id: &cpal::StreamId) {
        debug!("Playing stream with id {:?}", id);
        self.backend_loop.play_stream(id.clone());
    }

    pub fn pause_stream(&self, id: &cpal::StreamId) {
        debug!("Pausing stream with id {:?}", id);
        self.backend_loop.pause_stream(id.clone());
    }

    pub fn destroy_stream(&self, id: &cpal::StreamId) {
        debug!("Destroying stream with id {:?}", &id);
        self.backend_loop.destroy_stream(id.clone());
        // TODO: How to destroy the data?
    }

    pub fn get_backend_loop(&self) -> BackendLoop {
        Arc::clone(&self.backend_loop)
    }

    pub fn start_audio_thread(&self, streams: &SharedStreamMap) {
        let worker_event_loop = self.get_backend_loop();
        let audio_streams = Arc::clone(streams);
        thread::spawn(move || {
            let inner_streams = Arc::clone(&audio_streams);
            debug!("Initializing audio worker...");
            worker_event_loop.as_ref().run(move |stream_id, data| {
                AudioProcessor::process_audio(stream_id, data, inner_streams.clone());
            });
        });
    }

    // TODO: If this works we can refactor this function and simplify it more
    fn process_audio(id: cpal::StreamId, stream_data: cpal::StreamData, map: SharedStreamMap) {
        if !map.read().unwrap().contains(&id) {
            return;
        }
        let streams = map.read().unwrap();

        if let Ok(consumer) = streams.consumer(&id) {
            match stream_data {
                cpal::StreamData::Output {
                    buffer: cpal::UnknownTypeOutputBuffer::U16(mut _output),
                } => unimplemented!(),
                cpal::StreamData::Output {
                    buffer: cpal::UnknownTypeOutputBuffer::I16(mut output),
                } => {
                    let input = match consumer {
                        AudioConsumer::Signed(ref buffer) => buffer,
                        _ => panic!("Invalid consumer"),
                    };
                    let output_buffer = &mut *output;
                    input.read_blocking(output_buffer);
                }
                cpal::StreamData::Output {
                    buffer: cpal::UnknownTypeOutputBuffer::F32(mut output),
                } => {
                    let input = match consumer {
                        AudioConsumer::Float(ref buffer) => buffer,
                        _ => panic!("Invalid consumer"),
                    };
                    let output_buffer = &mut *output;
                    input.read_blocking(output_buffer);
                }
                _ => (),
            }
        }
    }

    /*
        AudioStreams::get().access_stream(&id, |stream| match stream_data);
        */
}
