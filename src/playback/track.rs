use playback::{Service, Metadata, MetadataReader, MetadataKey};
use serde_json;
use uuid;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Track {
    pub service: Service,
    pub metadata: Metadata,
    pub uuid: Option<uuid::Uuid>,
}

impl Track {
    pub fn list_from_slice(data: &[u8]) -> serde_json::Result<Vec<Track>> {
        serde_json::from_slice(data.as_ref())
    }

    pub fn get_service(&self) -> Service {
        self.service.clone()
    }

    pub fn get_uri(&self) -> String {
        self.metadata.get_string(MetadataKey::Uri).unwrap()
    }

    pub fn new(service: Service, data: Metadata, uuid: Option<uuid::Uuid>) -> Track {
        Track {
            service: service,
            metadata: data,
            uuid: uuid
        }
    }
}

impl Default for Track {
    fn default() -> Track {
        Track {
            service: Service::Local,
            metadata: Metadata::default(),
            uuid: None
        }
    }
}
