/* Communication */
pub use communication::events::{Event, RaiseEvent};
pub use communication::management::{self, channel_table, future_event_channel, temp_channel, Channel, ChannelTable};
pub use communication::signals::{signal_pair, Signal};
pub use derives::{FromJson, ToJson};

use crossbeam_channel as message;
use futures::sync as task_sync;
use metrohash::MetroHash64;
use serde_json;
use std::env;
use std::fmt::Debug;
use std::hash::Hash;
use std::hash::Hasher;
use std::path::Path;
use std::result::Result;
use std::sync::{Arc, Mutex, MutexGuard};
use std::thread;
pub type EventReceiver = message::Receiver<Event>;
pub type EventSender = message::Sender<Event>;
pub type TaskEventReceiver = task_sync::mpsc::UnboundedReceiver<Event>;
pub type TaskEventSender = task_sync::mpsc::UnboundedSender<Event>;
pub type TaskSignal = task_sync::oneshot::Receiver<()>;
pub type ThreadHandle = thread::JoinHandle<()>;

#[inline]
pub fn fast_hash<T>(input: T) -> u64
where
    T: Hash,
{
    let mut hasher = MetroHash64::default();
    input.hash(&mut hasher);
    hasher.finish()
}

pub trait UnwrapOrLogError<T, E> {
    fn unwrap_or_log_error(self, msg: &str) -> T;
}

pub trait AsTypeOrDefault {
    fn as_string_or_default(&self) -> String;
    fn as_u64_or_default(&self) -> u64;
}

pub trait MutexBooking<T: ?Sized> {
    fn book(&self) -> MutexGuard<T>;
}

impl<T: ?Sized> MutexBooking<T> for Mutex<T> {
    fn book(&self) -> MutexGuard<T> {
        match self.lock() {
            Ok(guard) => guard,
            Err(_) => panic!("Mutex is poisoned! State could be consistent"),
        }
    }
}

impl<T: Default, E: Debug> UnwrapOrLogError<T, E> for Result<T, E> {
    fn unwrap_or_log_error(self, msg: &str) -> T {
        match self {
            Ok(t) => t,
            Err(e) => {
                error!("{} -> {:#?}", msg, e);
                Default::default()
            }
        }
    }
}

impl AsTypeOrDefault for serde_json::Value {
    fn as_string_or_default(&self) -> String {
        String::from(self.as_str().unwrap_or_default())
    }

    fn as_u64_or_default(&self) -> u64 {
        self.as_u64().unwrap_or_default()
    }
}

#[inline]
pub fn extension_from_path(path: String) -> Result<String, ()> {
    let output = Path::new(&path).extension().unwrap_or_default().to_os_string();

    if output.is_empty() {
        Err(())
    } else {
        output.into_string().map_err(|_| ())
    }
}

#[inline]
pub fn named_thread<F>(name: &str, function: F) -> ThreadHandle
where
    F: FnOnce() -> (),
    F: Send + 'static,
{
    thread::Builder::new().name(name.to_string()).spawn(function).unwrap()
}

#[inline]
pub fn env_is_set(key: &str) -> bool {
    match env::var(key) {
        Ok(_) => true,
        Err(_) => false,
    }
}
