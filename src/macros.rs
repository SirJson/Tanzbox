#[macro_export]
macro_rules! module_id {
    () => {
        ::prelude::hash_str(module_path!())
    };
}

#[macro_export]
macro_rules! trace_event {
    ($event:expr) => {
        use $crate::communication::events::Event;
        match $event {
            Event::TrackStateUpdate { .. } | Event::Null => (),
            _ => trace!("Received Event: {}", $event.as_ref()),
        }
    };
}

#[macro_export]
macro_rules! string_vec {
    ($($element:expr),*) => {
        {
            let mut v = Vec::new();
            $(
                v.push(format!("{}", $element));
            )*
            v
        }
    };
}
