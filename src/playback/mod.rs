pub mod database;
pub mod decoder;
pub mod metadata;
pub mod player;
pub mod processor;
pub mod scheduler;
pub mod services;
pub mod streams;
pub mod track;

use cpal;
pub use playback::decoder::InetAudioDecoder;
pub use playback::metadata::FromSpotify as MetadataFromSpotify;
pub use playback::metadata::FromYoutubeDl as MetadataFromYoutubeDl;
pub use playback::metadata::{Metadata, MetadataKey, MetadataReader};
pub use playback::player::AudioServiceInstance;
pub use playback::player::PlayQueue;
pub use playback::player::Player;
pub use playback::player::PlayerInfo;
pub use playback::player::Status as PlayerStatus;
pub use playback::processor::AudioProcessor;
pub use playback::services::AudioService;
pub use playback::services::Service;
pub use playback::track::Track;
use std::sync::Arc;

type BackendLoop = Arc<cpal::EventLoop>;

pub fn init_backend() -> BackendLoop {
    info!("Initialize backend event loop...");
    Arc::new(cpal::EventLoop::new())
}
