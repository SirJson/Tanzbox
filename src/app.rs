use config;
use ctrlc;
use flexi_logger::{self, Logger};
use log::Record;
use prelude::*;
use std::io;
use std::time::{Duration, SystemTime};

static mut SYS_TIME: Option<SystemTime> = None;
const NANOS_PER_MILLI: u32 = 1_000_000;
pub static VERSION: &'static str = env!("CARGO_PKG_VERSION");
pub static NO_COLOR_LOG: Option<&'static str> = option_env!("TBOX_NO_COLOR");

pub struct Application {
    event: EventReceiver,
    pub config: config::Root,
}

pub fn uptime() -> Duration {
    unsafe {
        match SYS_TIME {
            Some(ref time) => time.elapsed().unwrap(),
            None => Duration::new(0, 0),
        }
    }
}

fn log_format(writer: &mut io::Write, record: &Record) -> Result<(), io::Error> {
    let elapsed = uptime();
    let loglvl = record.level();
    let path = record.module_path().unwrap_or("<unnamed>");
    let mut head = "";
    let mut tail = "";
    let mut green = "";

    if !NO_COLOR_LOG.is_some() {
        head = match &loglvl {
            flexi_logger::Level::Trace => "\x1B[0;32m",
            flexi_logger::Level::Debug => "\x1B[0;36m",
            flexi_logger::Level::Info => "\x1B[0;34m",
            flexi_logger::Level::Warn => "\x1B[0;33m",
            flexi_logger::Level::Error => "\x1B[0;31m",
        };
        green = "\x1B[0;32m";
        tail = "\x1B[0m";
    }

    write!(
        writer,
        "{col}{level:<5} {timecol}({time}){colend} | [{module}] {file}:{line} => \"{message}\"",
        col = head,
        timecol = green,
        time = format_args!("{}.{:03}", elapsed.as_secs(), elapsed.subsec_nanos() / NANOS_PER_MILLI),
        level = loglvl,
        module = path,
        message = &record.args(),
        colend = tail,
        file = record.file().unwrap_or("<unnamed>"),
        line = record.line().unwrap_or(0),
    )
}

#[cfg(build = "debug")]
pub fn setup_logger() {
    println!("\x1B[37;41mTanzbox {} {}\x1B[0m", VERSION, "Debug");
    let default_cfg = "tanzbox_server=debug";
    Logger::with_env_or_str(default_cfg)
        .format(log_format)
        .start()
        .unwrap_or_else(|e| panic!("Logger initialization failed with {}", e));
}

#[cfg(build = "release")]
pub fn setup_logger() {
    println!("Tanzbox {}", VERSION);
    let default_cfg = "tanzbox_server=info,librespot::core=warn,librespot::audio=warn,librespot::playback=warn";
    if std::prelude::env_is_set("TANZBOX_LOGFILE") {
        Logger::with_env_or_str(default_cfg)
            .log_to_file()
            .directory("logs")
            .print_message()
            .duplicate_info()
            .format(log_format)
            .start()
            .unwrap_or_else(|e| panic!("Logger initialization failed with {}", e));
    } else {
        Logger::with_env_or_str(default_cfg)
            .format(log_format)
            .start()
            .unwrap_or_else(|e| panic!("Logger initialization failed with {}", e));
    }
}

impl Application {
    pub fn new() -> Application {
        unsafe {
            SYS_TIME = Some(SystemTime::now());
        }
        setup_logger();
        let config = config::load("tanzbox.toml");
        let channels = channel_table();

        ctrlc::set_handler(move || {
            let channel = &channels.sender(Channel::Kernel);
            channel.send(Event::Halt);
        }).expect("Failed to setup SIGTERM handler");

        Application {
            event: channels.receiver(Channel::Kernel),
            config: config,
        }
    }

    fn shutdown(&mut self) {
        let broker = channel_table();
        broker.broadcast(Event::Shutdown);
    }

    pub fn run(mut self) {
        info!("Tanzbox ready to rock!");
        loop {
            let signal = self.event.recv().unwrap_or(Event::Null);
            trace_event!(signal);

            match signal {
                Event::Halt => {
                    self.shutdown();
                    break;
                }
                Event::Shutdown => break, // If we get the shutdown from a external source we assume our world is on fire
                _ => warn!("Application controller received unknown event: {:?}", signal),
            }
        }
    }
}
