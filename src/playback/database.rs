use playback::{AudioServiceInstance, Metadata, MetadataKey};
use serde_json;
use std::collections::HashMap;
use std::fs::OpenOptions;
use std::io;

pub static DB_FILE: &'static str = "database.json";

#[derive(Serialize, Deserialize)]
pub struct Metastore {
    data: HashMap<String, Metadata>,
}

impl Metastore {
    pub fn new() -> Metastore {
        Metastore { data: HashMap::new() }
    }

    pub fn get(&self, uri: &String) -> Option<Metadata> {
        if let Some(metadata) = self.data.get(uri) {
            Some(metadata.clone())
        } else {
            None
        }
    }

    pub fn load() -> io::Result<Metastore> {
        let file = OpenOptions::new().read(true).open(DB_FILE)?;
        let data = serde_json::from_reader(&file)?;
        Ok(data)
    }

    pub fn save(&self) -> io::Result<()> {
        let file = OpenOptions::new().write(true).create(true).truncate(true).open(DB_FILE)?;
        serde_json::to_writer_pretty(&file, &self)?;
        file.sync_all()?;
        Ok(())
    }

    pub fn insert(&mut self, input: &Metadata) {
        if !input.contains_key(&MetadataKey::Uri) {
            error!("Failed to insert Metadata into Database. Reason: URI missing: {:?}", input);
            return;
        }

        let key = input.get(&MetadataKey::Uri).unwrap().clone();
        self.data.insert(key, input.clone());
    }

    pub fn contains(&self, uri: &String) -> bool {
        self.data.contains_key(uri)
    }

    pub fn query(&mut self, uri: &String, service: &AudioServiceInstance) -> Metadata {
        if !self.contains(uri) {
            debug!("Requesting unknown URI \"{}\" querying service...", uri);
            let result = service.borrow().get_metadata(uri);
            self.insert(&result);
        } else {
            debug!("Found Metadata for URI \"{}\" in database!", uri);
        }

        self.get(uri).unwrap()
    }
}
