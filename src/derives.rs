use serde::Deserialize;

pub trait ToJson {
    fn to_json(&self) -> String;
}

pub trait FromJson {
    fn from_json_str<'a, T>(s: &'a str) -> T
    where
        T: Deserialize<'a> + Default;

    fn from_json_string<'a, T>(s: &'a String) -> T
    where
        T: Deserialize<'a> + Default;
}
