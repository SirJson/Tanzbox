use config;
use crossbeam_channel as message;
use playback::database::Metastore;
use playback::metadata::{MetadataKey, MetadataReader};
use playback::scheduler;
use playback::services::{LocalFiles, SoundCloud};
use playback::streams::{SharedStreamMap, StreamId, StreamMap};
use playback::{AudioProcessor, AudioService, Metadata, Service, Track};
use prelude::*;
use serde_json;
use sockets::protocol::{SearchQuery, TrackRequest};
use std::cell::RefCell;
use std::collections::HashMap;
use std::rc::Rc;
use std::thread;
use std::time::Duration;
use uuid;

pub type PlayQueue = Vec<Track>;
pub type AudioServiceInstance = Box<RefCell<AudioService>>;
type ServiceMap = HashMap<Service, AudioServiceInstance>;

#[derive(Debug)]
pub enum PlayerError {
    NotReady,
    NoService,
    NoTracksLeft,
    UnknownIndex,
    InvalidState,
}

#[derive(PartialEq, Eq, Serialize, Deserialize, Clone, Copy, Debug)]
pub enum Status {
    Unknown = 0x00,
    Play = 0x01,
    Pause = 0x02,
    Stop = 0x03,
}

impl Default for Status {
    fn default() -> Status {
        Status::Unknown
    }
}

#[derive(Serialize, Deserialize, Clone, ToJson, Default, Debug)]
pub struct PlayerInfo {
    queue: PlayQueue,
    current_track_index: u32,
    current_track_id: uuid::Uuid,
    status: Status,
    time_left: u32,
    time_elapsed: u32,
    time_total: u32,
}

pub struct Player {
    info: PlayerInfo,
    services: ServiceMap,
    sockets_channel: EventSender,
    scheduler_channel: EventSender,
    scheduler: ThreadHandle,
    receiver: EventReceiver,

    stream: Option<StreamId>,
    audio_processor: Rc<AudioProcessor>,
    metadata_store: Metastore,
    audio_streams: SharedStreamMap,
}

impl Player {
    pub fn start(main_config: &config::Playback) -> thread::JoinHandle<()> {
        debug!("Initializing Player...");
        let channels = channel_table();
        let player_receiver = channels.receiver(Channel::Player);
        let config = main_config.clone();

        named_thread("player_thread", move || {
            let mut service_map: ServiceMap = HashMap::new();



            let processor = Rc::new(AudioProcessor::create());
            let streams = StreamMap::shared();
            processor.start_audio_thread(&streams);
/*
            if config.spotify.enabled {
                service_map.insert(
                    Service::Spotify,
                    Box::new(RefCell::new(Spotify::new(config.spotify.clone(), Arc::clone(&_barrier)))),
                );
            }*/
            service_map.insert(
                Service::SoundCloud,
                Box::new(RefCell::new(SoundCloud::new(config.clone()))),
            );
            service_map.insert(Service::Local, Box::new(RefCell::new(LocalFiles::new())));

            let metastore = match Metastore::load() {
                Ok(data) => data,
                Err(_) => Metastore::new(),
            };

            let player = Player {
                info: PlayerInfo {
                    queue: PlayQueue::new(),
                    status: Status::Stop,
                    current_track_index: 0,
                    current_track_id: uuid::Uuid::default(),
                    time_left: 0,
                    time_elapsed: 0,
                    time_total: 0,
                },
                services: service_map,
                sockets_channel: channels.sender(Channel::WebSockets),
                scheduler_channel: channels.sender(Channel::PlaybackScheduler),
                scheduler: scheduler::run(),
                receiver: player_receiver,
                audio_processor: processor,
                stream: None,
                metadata_store: metastore,
                audio_streams: streams,
            };
            player.run();
        })
    }

    fn run(mut self) {
        loop {
            let signal = self.receiver.recv().unwrap_or(Event::Null);
            trace_event!(signal);
            match signal {
                Event::Shutdown => {
                    self.shutdown();
                    info!("Player shutdown!");
                    break;
                }
                Event::RequestPlayQueue(channel) => channel.send(self.get_queue().clone()),
                Event::RequestPlayerInfo(channel) => channel.send(self.get_info().clone()),
                Event::RequestSoundcloudMeta(channel, url) => channel.send(self.fetch_soundcloud_meta(url)),
                Event::PlayQueueStart(id) => self.play_by_id(id),
                Event::PlayQueueAdd(request) => self.queue_add(request),
                Event::PlayQueueRemove(index) => self.queue_remove(index),
                Event::PlayQueueClear => self.queue_clear(),
                Event::SkipTrack => self.skip_track(),
                Event::PauseTrack => self.pause().unwrap_or_log_error("Failed to pause current track"),
                Event::ResumeTrack => self.resume().unwrap_or_log_error("Failed to resume current track"),
                Event::TrackStateUpdate { left, initial, elapsed } => self.update_track_state(left, initial, elapsed),
                Event::MetadataUpdate(data) => self.scheduler_channel.send(Event::MetadataUpdate(data)),
                Event::TrackFinished => self.handle_track_finished(),
                Event::Search(query, caller) => self.handle_search_request(query, caller),
                _ => warn!("Kernel received signal that it can't handle. Signal = {:?}", signal),
            }
        }
    }

    fn handle_search_request(&mut self, query: SearchQuery, caller: message::Sender<Vec<Metadata>>) {
        debug!("handle_search_request > service_by_query");
        let service_query = self.services.service_by_query(&query);
        if service_query.is_err() {
            caller.send(Vec::new());
            warn!("Result for search query {:?} was empty", query);
            return;
        }

        //TODO: Erst Cache prüfen dann Query an Service machen
        let service = service_query.unwrap();
        debug!("searching...");
        let results = service.borrow().search(&query);
        let mut data: Vec<Metadata> = Vec::new();
        for result in results {
            debug!("Querying \"{}\"", result);
            data.push(self.metadata_store.query(&result, service));
        }

        debug!("send back...");
        caller.send(data);
    }

    fn handle_track_finished(&mut self) {
        self.try_play_next();
    }

    fn update_track_state(&mut self, left: Duration, initial: Duration, elapsed: Duration) {
        self.info.time_left = left.as_secs() as u32;
        self.info.time_total = initial.as_secs() as u32;
        self.info.time_elapsed = elapsed.as_secs() as u32;
        self.sockets_channel.send(Event::TrackStateUpdate {
            left: left,
            initial: initial,
            elapsed: elapsed,
        })
    }

    fn shutdown(self) {
        info!("Shutdown playback services...");
        for service in self.services.values() {
            service.borrow_mut().shutdown();
        }
        if let Err(error) = self.scheduler.join() {
            error!("Could not shutdown track scheduler: {:?}", error)
        }
        if let Err(error) = self.metadata_store.save() {
            error!("Saving database failed: {:?}", error);
        }
    }

    fn get_queue(&self) -> &PlayQueue {
        &self.info.queue
    }

    fn get_info(&self) -> &PlayerInfo {
        &self.info
    }

    fn fetch_soundcloud_meta(&self, url: String) -> String {
        if let Some(entry) = self.services.get(&Service::SoundCloud) {
            return entry.borrow().get_metadata(&url).to_json();
        }
        warn!("Failed to get soundcloud metadata!");
        String::from("{}")
    }

    fn skip_track(&mut self) {
        let next_track = self.info.current_track_index + 1;

        if next_track as usize >= self.info.queue.len() {
            self.stop();
            return;
        }

        self.play_by_index(next_track);
    }

    fn queue_add(&mut self, request: TrackRequest) {
        let data = self.metadata_store.get(&request.uri).unwrap();
        let track = Track::new(request.service, data, Some(uuid::Uuid::new_v4()));
        self.info.queue.push(track);
        debug!("Track pushed into queue!");
        self.sockets_channel.send(Event::QueueModified(self.info.queue.clone()));
    }

    fn queue_clear(&mut self) {
        self.stop();

        self.info.queue.truncate(0);
        self.sockets_channel.send(Event::QueueModified(self.info.queue.clone()));
    }

    fn queue_remove(&mut self, track_index: u32) {
        info!("Removing {} from queue...", track_index);

        debug!("Rebuild indicies...");
        let track_uuid = self.current_track_uuid();
        let next_track = self.info.current_track_index; // This index might be our next song

        debug!("Current song index = {}, Uuid = {}", self.info.current_track_index, track_uuid);
        self.info.queue.remove(track_index as usize);
        let search_result = self.info.queue.iter().position(|ref track| track.uuid.unwrap() == track_uuid);

        if let Some(new_index) = search_result {
            self.info.current_track_index = new_index as u32;
            self.info.current_track_id = track_uuid;
            info!("Moved current song to = {}, Uuid = {}", self.info.current_track_index, track_uuid);
        } else {
            let tracks_left = self.info.queue.len() as u32;
            info!("Tracks left: {}, Next song index would be: {}", tracks_left, next_track);
            if next_track >= tracks_left {
                info!("No tracks left. Stopping playback");
                self.stop();
            } else {
                info!("Playing next song...");
                self.play_by_index(next_track);
            }
        }

        self.sockets_channel.send(Event::QueueModified(self.info.queue.clone()));
    }

    fn current_track_uuid(&self) -> uuid::Uuid {
        self.info.queue.get(self.info.current_track_index as usize).unwrap().uuid.unwrap().clone()
    }

    fn current_service(&self) -> Option<Service> {
        let request = self.info.queue.get(self.info.current_track_index as usize);
        match request {
            Some(track) => Some(track.get_service()),
            None => None,
        }
    }

    fn play_by_id(&mut self, id: uuid::Uuid) {
        let (index, track) = match self.info.queue.find_by_id(id) {
            Some(result) => result,
            None => {
                error!("No Track found with id: {}", id);
                return;
            }
        };
        if let Err(error) = self.play_track(index, &track) {
            error!("Failed to play by id. Reason: {:?}", error);
        }
    }

    fn play_by_index(&mut self, index: u32) {
        let result = self.info.queue.find_by_index(index);
        if result.is_none() {
            error!("No Track found with index: {}", index);
        } else {
            let (idx, track) = result.unwrap();
            if let Err(error) = self.play_track(idx, &track) {
                error!("Failed to play by index. Reason: {:?}", error);
            }
        }
    }

    fn play_track(&mut self, index: usize, track: &Track) -> Result<(), PlayerError> {
        self.stop();

        let instance: Option<&AudioServiceInstance> = self.services.service_by_track(track).ok();
        if instance.is_none() {
            return Err(PlayerError::NoService);
        }

        let service = instance.unwrap();

        self.stream = Some(self.begin_stream(service, track));
        let id = track.uuid.unwrap();
        self.info.current_track_index = index as u32;
        self.info.current_track_id = id;
        self.info.status = Status::Play;
        self.sockets_channel.send(Event::PlayTrack(id));
        self.scheduler_channel.send(Event::PlayTrack(id));
        Ok(())
    }

    fn begin_stream(&self, instance: &AudioServiceInstance, track: &Track) -> StreamId {
        let prefered_sample_format = instance.borrow().prefered_sample_format();
        let prefered_sample_rate = instance.borrow().prefered_sample_rate();

        let sample_format = if prefered_sample_format.is_none() {
            self.audio_processor.default_sample_format()
        } else {
            prefered_sample_format.unwrap()
        };

        let sample_rate = if prefered_sample_rate.is_none() {
            self.audio_processor.default_sample_rate()
        } else {
            prefered_sample_rate.unwrap()
        };

        //TODO: Make Channels configurable
        let id = {
            self.audio_streams.write().unwrap().spawn(
                self.audio_processor.device(),
                self.audio_processor.get_backend_loop(),
                sample_format,
                sample_rate,
                2,
                track.metadata.get_number(MetadataKey::Duration).unwrap_or(0),
            )
        };

        self.scheduler_channel.send(Event::MetadataUpdate(track.metadata.clone()));
        debug!("Waiting for scheduler metadata update and track preload...");
        let audio_format = { self.audio_streams.read().unwrap().audio_format(&id).unwrap().clone() };
        instance.borrow_mut().load(&track.get_uri(), &audio_format);
        self.audio_processor.play_stream(&id);
        instance.borrow_mut().play( &audio_format);
        debug!("All synced up! Beginning with playing the track");
        id
    }

    fn play_next(&mut self) -> Result<(), PlayerError> {
        let tracks_left = self.info.queue.len() as u32;
        if tracks_left == 0 {
            return Err(PlayerError::NoTracksLeft);
        }
        let next_track = self.info.current_track_index + 1;
        if next_track >= tracks_left {
            return Err(PlayerError::NoTracksLeft);
        }
        self.play_by_index(next_track);
        Ok(())
    }

    fn try_play_next(&mut self) {
        self.stop();
        match self.play_next() {
            Ok(()) => info!("Playing next song..."),
            Err(PlayerError::NoTracksLeft) => info!("No tracks left"),
            Err(error) => error!("Couldn't play the next track: {:?}", error),
        }
    }

    fn stop(&mut self) {
        if self.info.status == Status::Stop {
            return;
        }
        if let Some(service) = self.current_service() {
            self.stop_service(service)
                .expect(format!("Couldn't stop service: {}. State is now inconsistent!", service).as_str());
            self.info.status = Status::Stop;
            self.sockets_channel.send(Event::StopTrack);
            self.scheduler_channel.send(Event::StopTrack);
            self.info.current_track_id = uuid::Uuid::default();
            self.info.current_track_index = 0;
            let id = self.stream.as_ref().unwrap();
            self.audio_processor.destroy_stream(&id);
        } else {
            error!("Can't stop player because no playback service is loaded")
        }
        self.stream = None;
    }

    fn pause(&mut self) -> Result<(), PlayerError> {
        if self.info.status != Status::Play {
            return Err(PlayerError::InvalidState);
        }
        match self.info.queue.get(self.info.current_track_index as usize) {
            Some(track) => {
                debug!("Pausing track: {:?}", &track);
                let service_entry = self.services.get(&track.get_service());
                match service_entry {
                    Some(entry) => {
                        if entry.borrow().is_ready() == false {
                            warn!(
                                "Can't pause track with uri {}! Service {:?} is not ready",
                                track.get_uri(),
                                track.get_service()
                            );
                            return Err(PlayerError::NotReady);
                        }
                        self.info.status = Status::Pause;
                        self.audio_processor.pause_stream(self.stream.as_ref().unwrap());
                        self.sockets_channel.send(Event::PauseTrack);
                        self.scheduler_channel.send(Event::PauseTrack);
                        Ok(entry.borrow().pause())
                    }
                    None => {
                        error!(
                            "Can't pause track with uri {}! Service {:?} is not loaded",
                            track.get_uri(),
                            track.get_service()
                        );
                        Err(PlayerError::NoService)
                    }
                }
            }
            None => Err(PlayerError::UnknownIndex),
        }
    }

    fn stop_service(&mut self, service: Service) -> Result<(), PlayerError> {
        if self.info.status == Status::Stop {
            return Ok(());
        }
        let service_entry = self.services.get(&service);
        match service_entry {
            Some(entry) => {
                if entry.borrow().is_ready() == false {
                    warn!("Can't stop playback! Service {:?} is not ready", service);
                    return Err(PlayerError::NotReady);
                }
                self.info.status = Status::Stop;
                Ok(entry.borrow().stop())
            }
            None => {
                error!("Can't stop playback! Service {:?} is not loaded", service);
                Err(PlayerError::NoService)
            }
        }
    }

    fn resume(&mut self) -> Result<(), PlayerError> {
        if self.info.status != Status::Pause {
            return Err(PlayerError::InvalidState);
        }
        match self.info.queue.get(self.info.current_track_index as usize) {
            Some(track) => {
                let service_entry = self.services.get(&track.get_service());
                match service_entry {
                    Some(entry) => {
                        if entry.borrow().is_ready() == false {
                            warn!(
                                "Can't resume track with uri {}! Service {:?} is not ready",
                                track.get_uri(),
                                track.get_service()
                            );
                            return Err(PlayerError::NotReady);
                        }
                        self.info.status = Status::Play;
                        self.audio_processor.play_stream(self.stream.as_ref().unwrap());
                        self.sockets_channel.send(Event::PlayTrack(self.current_track_uuid()));
                        self.scheduler_channel.send(Event::ResumeTrack);
                        Ok(entry.borrow().resume())
                    }
                    None => {
                        error!(
                            "Can't resume track with uri {}! Service {:?} is not loaded",
                            track.get_uri(),
                            track.get_service()
                        );
                        Err(PlayerError::NoService)
                    }
                }
            }
            None => Err(PlayerError::UnknownIndex),
        }
    }
}

trait ServiceMapExt {
    fn service_by_track(&self, track: &Track) -> Result<&AudioServiceInstance, PlayerError>;
    fn service_by_query(&self, query: &SearchQuery) -> Result<&AudioServiceInstance, PlayerError>;
}

trait PlayQueueExt {
    fn find_by_id(&self, id: uuid::Uuid) -> Option<(usize, Track)>;
    fn find_by_index(&self, index: u32) -> Option<(usize, Track)>;
}

impl PlayQueueExt for PlayQueue {
    fn find_by_id(&self, id: uuid::Uuid) -> Option<(usize, Track)> {
        let output = self
            .iter()
            .enumerate()
            .find(|&x| if x.1.uuid.is_none() { return false } else { x.1.uuid.unwrap() == id });
        if output.is_none() {
            error!("No track found with id: {:?}", id);
            return None;
        }
        let (index, track) = output.unwrap();
        Some((index, track.clone()))
    }

    fn find_by_index(&self, index: u32) -> Option<(usize, Track)> {
        let idx = index as usize;
        let track = match self.get(idx) {
            Some(result) => result,
            None => {
                error!("Failed to find Track with index: {}", index);
                return None;
            }
        };

        Some((idx, track.clone()))
    }
}

impl ServiceMapExt for ServiceMap {
    fn service_by_track(&self, track: &Track) -> Result<&AudioServiceInstance, PlayerError> {
        let service_entry = self.get(&track.get_service());
        match service_entry {
            Some(entry) => {
                if entry.borrow().is_ready() == false {
                    warn!("Service {:?} is not ready", track.get_service());
                    return Err(PlayerError::NotReady);
                }
                Ok(entry)
            }
            None => {
                error!("Service {:?} is not loaded", track.get_service());
                Err(PlayerError::NoService)
            }
        }
    }

    fn service_by_query(&self, query: &SearchQuery) -> Result<&AudioServiceInstance, PlayerError> {
        match self.get(&query.service) {
            Some(entry) => Ok(entry),
            None => {
                warn!("Tried to search Track for Service {:?} but Service is not implemented", query.service);
                Err(PlayerError::NoService)
            }
        }
    }
}
