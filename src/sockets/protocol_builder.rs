#[macro_export]
macro_rules! build_protocol {
    ( [$name:ident, version: $version:expr] => $( {($cmd:ident, $id:expr) => [ $( ($varname:ident:$type:ty) ),* ]} ),* ) => {
        #[allow(dead_code)]
        pub mod $name {
            use derives::{ToJson, FromJson};
            use serde_json;
            use serde::Deserialize;

            #[derive(Clone, Debug)]
            pub enum Command
            {
                Invalid = 0x00,
                $(
                    $cmd = $id,
                )*
            }

            pub fn command(data: &serde_json::Value) -> Command
            {
                if let Some(cmd) = data["command"].as_u64() {
                    match cmd {
                        $(
                            $id => Command::$cmd,
                        )*
                        _ => {
                            warn!("Invalid command ID: {}", cmd);
                            Command::Invalid
                        }
                    }
                }
                else {
                    warn!("Failed to parse command: {:?}",data);
                    Command::Invalid
                }
            }

            pub fn valid_header(data: &serde_json::Value) -> bool
            {
                data["version"] == $version
            }

            $(
            #[derive(Serialize, Deserialize, Clone, Default, ToJson, FromJson, Debug)]
            pub struct $cmd
            {
                version: u8,
                command: u8,
                $(
                    pub $varname: $type,
                )*
            }

            #[allow(dead_code)]
            impl $cmd
            {
                pub fn new($($varname: $type,)*) -> $cmd
                {
                    $cmd {
                        version: $version,
                        command: $id,
                        $(
                            $varname: $varname,
                        )*
                    }
                }

                pub fn from_json_value(value: serde_json::Value) -> $cmd
                {
                    match serde_json::from_value(value) {
                        Ok(instance) => instance,
                        Err(error) => {
                            error!("Failed to deserialize JSON message: {}",error);
                            Default::default()
                        }
                    }
                }

                pub fn as_json($($varname: $type,)*) -> String
                {
                    let data = $cmd::new($($varname,)*);
                    data.to_json()
                }
            }

            )*
        }
    };
}
