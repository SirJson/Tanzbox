#[allow(unused_imports)]
use prelude::ToJson;
use serde_json;
use std::fs;
use std::fs::File;
use std::io::prelude::*;
use std::io::ErrorKind;
use std::path::Path;
use std::process::exit;
use toml;

#[derive(Serialize, Deserialize, Clone)]
pub struct Root {
    pub web_sockets: WebSockets,
    pub playback: Playback,
}

#[derive(Serialize, Deserialize, Clone)]
pub struct Playback {
    pub youtubedl_path: String,
    pub ffmpeg_path: String,
    pub spotify: Spotify,
}

#[derive(Serialize, Deserialize, Clone)]
pub struct Spotify {
    pub enabled: bool,
    pub username: String,
    pub password: String,
}

#[derive(Serialize, Deserialize, ToJson, Clone)]
pub struct WebSockets {
    pub address: String,
}

fn generate(cfg_path: &Path) -> ! {
    info!("Generating template config...");

    if cfg_path.is_file() {
        error!(
            "{} already exists! The server will exit now to prevent data loss. Consider generating a new config or fixing your current config.",
            cfg_path.display()
        );

        exit(0)
    }

    let config = Root {
        playback: {
            Playback {
                youtubedl_path: String::from("/usr/bin/youtube-dl"),
                ffmpeg_path: String::from("/usr/bin/ffmpeg"),
                spotify: Spotify {
                    enabled: false,
                    username: String::default(),
                    password: String::default(),
                },
            }
        },
        web_sockets: WebSockets {
            address: String::from("0.0.0.0:8842"),
        },
    };

    info!("Writing template config to: \"{}\"", cfg_path.display());
    let mut file = File::create(&cfg_path).expect("Failed to create config file");
    match toml::to_string(&config) {
        Ok(src) => file.write_all(src.as_bytes()).expect("Failed to write config"),
        Err(error) => {
            error!("Failed to serialize template config from code! TOML Error: {}", error);
            warn!("This means you will now find a corrupted or empty config file in your application folder. Please download the template config from Gitlab.");
            panic!("Fatal config generator error");
        }
    }
    info!("Template config successfuly generated!");
    warn!("To complete your config remember to fill out all blanks that we couldn't provide for you.");
    warn!("The server will exit now!");
    exit(0)
}

pub fn load(path_str: &str) -> Root {
    let cfg_path = match fs::canonicalize(path_str) {
        Ok(realpath) => realpath,
        Err(error) => match error.kind() {
            ErrorKind::NotFound => {
                warn!("It looks like you don't have a config file. We try to generate a template config for you...");
                generate(Path::new(path_str));
            }
            _ => {
                error!("Failed to resolve config path \"{}\". The server will exit now! ({:?})", path_str, error);
                panic!("Config path resolve error");
            }
        },
    };

    info!("Loading config file \"{}\"", cfg_path.display());
    let mut contents = String::new();
    match File::open(&cfg_path) {
        Ok(mut file) => {
            let bytes_read = file.read_to_string(&mut contents).unwrap_or(0);
            if bytes_read <= 0 {
                error!("Failed to read config file. Your config might be empty or corrupted.");
                panic!("Empty config file")
            }

            toml::from_str(&contents).unwrap_or_else(|err| {
                warn!("Failed to parse config file. Reason: {}", err);
                panic!("Config parse error")
            })
        }
        Err(error) => {
            error!("Failed to open config file. Reason: {}", error);
            panic!("General config file IO error");
        }
    }
}
