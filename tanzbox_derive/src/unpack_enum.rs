use proc_macro2::TokenStream;
use syn;

pub fn impl_unpack_enum(ast: &syn::DeriveInput) -> TokenStream {
    let name = &ast.ident;
    let variants = match ast.data {
        syn::Data::Enum(ref v) => &v.variants,
        _ => panic!("Display only works on Enums"),
    };

    let mut into_impls = Vec::new();
    for variant in variants {
        use syn::Fields::*;
        let ident = &variant.ident;
        let params: Vec<&syn::Field> = match variant.fields {
            Unnamed(ref fields) => fields.unnamed.iter().collect(),
            _ => panic!("Display only works on Unnamed Enums"),
        };
        for param in params {
            into_impls.push(quote!{
                impl ::std::convert::Into<Option<#param>> for #name {
                    fn into(self) -> Option<#param> {
                        match self {
                            #name::#ident(u) => Some(u),
                            _ => None,
                        }
                    }
                }
            });
        }
    }
    quote!{
        #(#into_impls)*
    }
}
