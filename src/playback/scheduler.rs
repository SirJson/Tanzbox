use playback::{Metadata, MetadataKey, MetadataReader};
use prelude::*;
use std::thread;
use std::time::Duration;

struct Hypervisor {
    track_playing: bool,
    track_duration: Duration,
    track_position: Duration,
    tick: Duration,
    current_metadata: Option<Metadata>,

    event_receiver: EventReceiver,
    player_channel: EventSender,
}

impl Hypervisor {
    pub fn start(mut self) {
        loop {
            if let Some(signal) = self.event_receiver.try_recv() {
                trace_event!(signal);
                match signal {
                    Event::MetadataUpdate(data) => {
                        self.track_duration = Duration::from_secs(data.get_number(MetadataKey::Duration).unwrap_or_default());
                        self.current_metadata = Some(data);
                        debug!("Scheduler is waiting for player...");
                    }
                    Event::PlayTrack(_) => {
                        self.track_playing = true;
                        self.track_position = Duration::from_secs(0);
                    }
                    Event::StopTrack => {
                        self.track_playing = false;
                        self.current_metadata = None;
                    }
                    Event::PauseTrack => self.track_playing = false,
                    Event::ResumeTrack => self.track_playing = true,
                    Event::Shutdown => {
                        info!("Track Scheduler shutdown!");
                        break;
                    }
                    _ => warn!("Received unknown signal: {:?}", signal),
                }
            }

            if self.track_playing && self.track_position <= self.track_duration {
                // let time_left: Duration;
                // match self.track_duration.checked_sub(self.track_position) {
                //     Some(time) => time_left = time,
                //     None => time_left = Duration::from_secs(0),
                // }
                // self.player_channel.raise(Event::TrackStateUpdate {
                //     left: time_left,
                //     initial: self.track_duration,
                //     elapsed: self.track_position
                // });
                self.track_position += self.tick;
            } else {
                if self.track_playing {
                    self.player_channel.send(Event::TrackFinished);
                    self.track_playing = false
                }
            }
            thread::sleep(self.tick);
        }
    }
}

pub fn run() -> ThreadHandle {
    let channels = channel_table();
    named_thread("track_scheduler", move || {
        let hypervisor = Hypervisor {
            track_playing: false,
            track_duration: Duration::from_secs(0),
            track_position: Duration::from_secs(0),
            tick: Duration::from_secs(1),
            current_metadata: None,
            player_channel: channels.sender(Channel::Player),
            event_receiver: channels.receiver(Channel::PlaybackScheduler),
        };

        hypervisor.start();
    })
}
