use config;
use playback::streams::{AudioFormat, SampleFormat};
use prelude::*;
use serde_json;
use serde_json::Value as JSONValue;
use std::fs;
use std::io::{BufRead, BufReader};
use std::path::PathBuf;
use std::process::{Command, Stdio};
use task::{Pipe, PipeConfig, RunCommand, StdioData, StdioType, Task};
use uuid;

pub const FFMPEG_FLOAT_TYPE: &'static str = "f32le";
pub const FFMPEG_INT_TYPE: &'static str = "s16le";
pub const FFMPEG_UINT_TYPE: &'static str = "u16le";

pub struct InetAudioDecoder {
    youtubedl_path: PathBuf,
    ffmpeg_path: PathBuf,

}

impl InetAudioDecoder {
    pub fn open(config: config::Playback) -> InetAudioDecoder {
        let ffmpeg = fs::canonicalize(&config.ffmpeg_path).expect(&format!("Failed to find ffmpeg at \"{}\"", &config.ffmpeg_path));
        let youtubedl = fs::canonicalize(&config.youtubedl_path).expect(&format!("Failed to find youtube-dl at \"{}\"", &config.youtubedl_path));
        InetAudioDecoder {
            youtubedl_path: youtubedl,
            ffmpeg_path: ffmpeg,

        }
    }

    fn play<T: StdioType>(&mut self, mut loader: Task, mut decoder: Task, format: &AudioFormat)
    where
        T: Sized + Send,
    {
        loader.exec::<T>(PipeConfig {
            stdin: Pipe::null(),
            stdout: Pipe::raw(),
            stderr: Pipe::async_text(),
        });
        decoder.exec::<T>(PipeConfig {
            stdin: Pipe::from(loader.take_stdout().expect("There is no stdout pipe for youtube-dl")),
            stdout: Pipe::async(),
            stderr: Pipe::async_text(),
        });

        debug!("Decoder ready!");
        let pcm_format = format.clone();

        debug!("Start decoding...");
        named_thread("play", move || {
            let stream_channel = channel_table().sender(Channel::Player);
            match pcm_format.data_type {
                SampleFormat::F32 => while decoder.alive() {
                    let mut chunk = Vec::new();
                    for sample in decoder.stdout() {
                        match sample {
                            StdioData::F32(val) => chunk.push(val),
                            _ => (),
                        }
                    }
                    stream_channel.send(Event::PCMF32Data(chunk));
                    for line in loader.stderr_text() {
                        debug!("Loader: {}", line);
                    }
                    for line in decoder.stderr_text() {
                        debug!("Decoder: {}", line);
                    }
                },
                SampleFormat::I16 => unimplemented!(),
                SampleFormat::U16 => unimplemented!(),
            }
        });
    }

    pub fn start(&mut self, url: &String, format: &AudioFormat) {
        let loader: Task = Task::new(RunCommand {
            program: String::from(self.youtubedl_path.to_string_lossy()),
            args: string_vec!["-o", "-", url],
        });
        match format.data_type {
            SampleFormat::F32 => {
                let mut decoder: Task = Task::new(RunCommand {
                    program: String::from(self.ffmpeg_path.to_string_lossy()),
                    args: string_vec![
                        "-f",
                        "mp3",
                        "-i",
                        "pipe:0",
                        "-ar",
                        format.sample_rate.0,
                        "-f",
                        FFMPEG_FLOAT_TYPE,
                        "pipe:1"
                    ],
                });
                self.play::<f32>(loader, decoder, format);
            }
            SampleFormat::I16 => {
                let mut decoder: Task = Task::new(RunCommand {
                    program: String::from(self.ffmpeg_path.to_string_lossy()),
                    args: string_vec!["-f", "mp3", "-i", "pipe:0", "-ar", format.sample_rate.0, "-f", FFMPEG_INT_TYPE, "pipe:1"],
                });
                self.play::<i16>(loader, decoder, format);
            }
            SampleFormat::U16 => {
                let mut decoder: Task = Task::new(RunCommand {
                    program: String::from(self.ffmpeg_path.to_string_lossy()),
                    args: string_vec!["-f", "mp3", "-i", "pipe:0", "-ar", format.sample_rate.0, "-f", FFMPEG_UINT_TYPE, "pipe:1"],
                });
                self.play::<u16>(loader, decoder, format);
            }
        };
    }

    pub fn get_metadata_raw(&self, url: &String) -> String {
        let args = ["-j", &url];
        let mut track_json: Vec<String> = Vec::new();
        let mut child = Command::new(&self.youtubedl_path)
            .args(&args)
            .stdout(Stdio::piped())
            .spawn()
            .expect("Failed to launch youtube-dl");
        if let Some(ref mut stdout) = child.stdout {
            let lines = BufReader::new(stdout).lines();
            for line in lines {
                track_json.push(line.unwrap_or_default());
            }
        }
        format!("[{}]", track_json.join(","))
    }

    pub fn get_metadata(&self, url: &String) -> JSONValue {
        let mut payload = JSONValue::Null;
        let response = self.get_metadata_raw(url);
        match serde_json::from_str(&response) {
            Ok(json) => payload = json,
            Err(error) => {
                let dump_filename = format!("youtube-dl_error-dump-{}.json", uuid::Uuid::new_v4());
                if let Err(fserr) = fs::write(&dump_filename, response) {
                    error!("Failed to write error dump file: {:?}", fserr);
                }
                error!("Could not parse youtube-dl json: {:?}, Error dump file: {}", error, dump_filename);
            }
        }

        payload
    }
}
