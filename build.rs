//rpi_deps/usr/lib/arm-linux-gnueabihf

use std::env;
use std::path::PathBuf;

fn main() {
    println!("cargo:rustc-cfg=term={:?}", "color");
    if let Ok(profile) = env::var("PROFILE") {
        println!("cargo:rustc-cfg=build={:?}", profile);
    }
    if let Ok(target) = env::var("TARGET") {
        if target == "armv7-unknown-linux-gnueabihf" {
            if let Ok(cross_deps) = env::var("DEPS") {
                let mut libs = PathBuf::from(cross_deps);
                libs.push("usr/lib/arm-linux-gnueabihf");

                println!("cargo:rustc-link-search={}", libs.display());
            }
        }
    }
}
