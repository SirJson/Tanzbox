use playback::PlayerStatus;
use prelude::*;
use sockets::protocol::client;
use sockets::protocol::PositionUpdate;
use ws::Sender;

pub struct Controller {
    broadcaster: Sender,
    channel: EventReceiver,
}

impl Controller {
    pub fn new(broadcaster_socket: Sender, channel: EventReceiver) -> Controller {
        Controller {
            broadcaster: broadcaster_socket,
            channel: channel,
        }
    }

    fn try_broadcast(&self, data: String) {
        match self.broadcaster.send(data) {
            Ok(_) => (),
            Err(error) => {
                warn!("Couldn't broadcast message! {}", error);
            }
        }
    }

    pub fn run(&mut self) {
        debug!("Starting WebSocket controller...");
        loop {
            let event = self.channel.recv().unwrap_or(Event::Null);
            trace_event!(event);
            match event {
                Event::Shutdown => {
                    info!("WebSocket controller shutdown!");
                    self.broadcaster.shutdown().unwrap();
                    break; // Shutdown the loop
                }
                Event::QueueModified(data) => {
                    self.try_broadcast(client::QueueDirty::as_json(data));
                }
                Event::PlayTrack(id) => {
                    self.try_broadcast(client::PlayTrack::as_json(id));
                }
                Event::StopTrack => {
                    self.try_broadcast(client::TrackStatusUpdate::as_json(PlayerStatus::Stop));
                }
                Event::PauseTrack => {
                    self.try_broadcast(client::TrackStatusUpdate::as_json(PlayerStatus::Pause));
                }
                Event::TrackStateUpdate { left:_ , initial, elapsed } => {
                    let update = PositionUpdate {
                        elapsed: elapsed.as_secs() as u32,
                        length: initial.as_secs() as u32
                    };
                    self.try_broadcast(client::TrackPositionUpdate::as_json(update));
                }
                _ => info!("Frontend controller received Event that it can't handle. Event = {:?}", event),
            }
        }
    }
}
