#!/bin/bash

curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt-get update -qq && sudo apt-get install -y -qq python3 curl build-essential gcc-multilib-arm-linux-gnueabihf yarn
curl https://sh.rustup.rs -sSf | sh -s -- --default-toolchain stable -y
rustup target add armv7-unknown-linux-gnueabihf
yarn global add @angular/cli
echo 'export PATH=$HOME/.yarn/bin:$PATH' >> ~/.bashrc
echo 'source $HOME/cargo/env' >> ~/.bashrc
