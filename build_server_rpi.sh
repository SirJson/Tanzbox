#!/bin/bash
if [ $1 = "deb_compiler" ]; then
  echo "Configure cargo to use cross compiler for linking..."
  yes | cp -f etc/cargo_debian_cross.toml $HOME/.cargo/config
fi

START=$PWD
export DEPS=$PWD/rpi_deps

echo "Building from $START"

if [ -d "$DEPS" ]; then
  rm -rf $DEPS
fi

mkdir $DEPS
cd $DEPS

wget -O libssl.deb "http://ftp.de.debian.org/debian/pool/main/o/openssl1.0/libssl1.0.2_1.0.2l-2+deb9u2_armhf.deb"
wget -O libssl-dev.deb "http://ftp.de.debian.org/debian/pool/main/o/openssl1.0/libssl1.0-dev_1.0.2l-2+deb9u2_armhf.deb"
wget -O libasound-dev.deb "http://ftp.de.debian.org/debian/pool/main/a/alsa-lib/libasound2-dev_1.1.3-5_armhf.deb"
wget -O libasound.deb "http://ftp.de.debian.org/debian/pool/main/a/alsa-lib/libasound2_1.1.3-5_armhf.deb"

dpkg -x libssl.deb .
dpkg -x libssl-dev.deb .
dpkg -x libasound.deb .
dpkg -x libasound-dev.deb .

cp $DEPS/usr/include/arm-linux-gnueabihf/openssl/opensslconf.h $DEPS/usr/include/openssl/opensslconf.h

cd $START

export ARMV7_UNKNOWN_LINUX_GNUEABIHF_OPENSSL_DIR=$DEPS/usr
export ARMV7_UNKNOWN_LINUX_GNUEABIHF_OPENSSL_LIB_DIR=$DEPS/usr/lib/arm-linux-gnueabihf
export ARMV7_UNKNOWN_LINUX_GNUEABIHF_OPENSSL_INCLUDE_DIR=$DEPS/usr/include
export OPENSSL_DIR=$DEPS/usr
export OPENSSL_LIB_DIR=$DEPS/usr/lib/arm-linux-gnueabihf
export OPENSSL_INCLUDE_DIR=$DEPS/usr/include
export ALSA_DIR=$DEPS/usr
export ALSA_LIB_DIR=$DEPS/usr/lib/arm-linux-gnueabihf
export ALSA_INCLUDE_DIR=$DEPS/usr/include

cargo build --release --target=armv7-unknown-linux-gnueabihf
