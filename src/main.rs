
extern crate bitflags;
extern crate byteorder;
extern crate cpal;
extern crate crossbeam_channel;
extern crate flexi_logger;
extern crate futures;
extern crate librespot;
#[macro_use]
extern crate log;
extern crate metrohash;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate serde_json;
extern crate strum;
#[macro_use]
extern crate strum_macros;
#[macro_use]
extern crate tanzbox_derive;
extern crate claxon;
extern crate ctrlc;
extern crate rb;
extern crate tokio_core;
extern crate toml;
extern crate uuid;
extern crate ws;

#[macro_use]
mod macros;
#[macro_use]
pub mod communication;
mod app;
mod config;
mod derives;
mod playback;
#[allow(unused_imports, dead_code)]
mod prelude;
mod sockets;
mod task;

use app::Application;
use communication::events::Event;
use communication::management::channel_table;
use playback::Player;
use std::panic;

fn register_panic_handler() {
    let default_handler = panic::take_hook();
    panic::set_hook(Box::new(move |panic_info| {
        default_handler(panic_info);
        error!("A thread crashed! The server will now try to free all resources and shutdown.");
        let broker = channel_table();
        broker.broadcast(Event::Shutdown);
    }));
}

fn main() {
    register_panic_handler();

    info!("Starting server...");

    let app = Application::new();
    let websockets_thread = sockets::start(&app.config.web_sockets);
    let player_thread = Player::start(&app.config.playback);

    app.run();

    if let Err(_) = websockets_thread.join() {
        error!("Frontend thread crashed");
    }

    if let Err(_) = player_thread.join() {
        error!("Player Thread crashed");
    }

    info!("Server shutdown complete!");
}
