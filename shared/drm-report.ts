
export class DRMReport
{
    Widevine: boolean;
    PlayReady: boolean;
    ClearKey: boolean;
    PluginInfos: string[];
}