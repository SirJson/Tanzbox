#![recursion_limit = "128"]

extern crate syn;
#[macro_use]
extern crate quote;
extern crate proc_macro;
extern crate proc_macro2;

mod json;
mod unpack_enum;

#[allow(unused_imports)]
use proc_macro2::TokenStream;

#[proc_macro_derive(ToJson)]
pub fn to_json_derive(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let ast = syn::parse(input).unwrap();
    let gen = json::impl_to_json(&ast);
    gen.into()
}

#[proc_macro_derive(FromJson)]
pub fn from_json_derive(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let ast = syn::parse(input).unwrap();
    let gen = json::impl_from_json(&ast);
    gen.into()
}

#[proc_macro_derive(UnpackEnum)]
pub fn unpack_enum_derive(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let ast = syn::parse(input).unwrap();
    let gen = unpack_enum::impl_unpack_enum(&ast);
    gen.into()
}
