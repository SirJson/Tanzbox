use claxon;
use playback::services::AudioService;
use playback::streams::{AudioFormat, SampleFormat};
use playback::{Metadata, MetadataKey, Service};
use sockets::protocol::SearchQuery;
use std::fs;
use std::io;
use std::path::PathBuf;

pub struct LocalFiles {
    library_path: PathBuf,
}

impl LocalFiles {
    pub fn new() -> LocalFiles {
        debug!("Initializing local file service...");
        LocalFiles {
            library_path: fs::canonicalize("./music").unwrap_or_default(),
        }
    }

    fn list(&self, search: &String) -> io::Result<Vec<String>> {
        let mut output: Vec<String> = Vec::new();
        if !self.library_path.is_dir() {
            return Err(io::Error::new(io::ErrorKind::InvalidData, "Provided path is not a directory"));
        }
        for entry in fs::read_dir(&self.library_path)? {
            let dir_entry = entry?;
            if !dir_entry.metadata()?.is_file() {
                continue;
            }
            let path = dir_entry.path();
            if let Some(ext) = path.extension() {
                match ext.to_str().unwrap_or_default() {
                    "flac" => {
                        let file = String::from(path.to_string_lossy());
                        if file.contains(search) {
                            output.push(file);
                        }
                    }
                    _ => warn!("Invalid file found in music library: '{:?}'", dir_entry.file_name()),
                }
            }
        }
        Ok(output)
    }
}

impl AudioService for LocalFiles {
    fn load(&mut self, _uri: &String, _format: &AudioFormat) {
        unimplemented!()
    }

    fn play(&mut self, format: &AudioFormat) {
        unimplemented!()
    }

    fn pause(&self) {}

    fn resume(&self) {}

    fn stop(&self) {}

    fn shutdown(&mut self) {}

    fn is_ready(&self) -> bool {
        true
    }

    fn get_metadata(&self, uri: &String) -> Metadata {
        let reader = claxon::FlacReader::open(uri).expect("failed to open FLAC stream");
        for (name, value) in reader.tags() {
            // Print comments in a format similar to what
            // `metaflac --block-type=VORBIS_COMMENT --list` would print.
            println!("{}: {}={}", uri, name, value);
        }
        let mut output = Metadata::new();

        output.insert(MetadataKey::Duration, "1000".to_string());
        output.insert(MetadataKey::Title, reader.get_tag("title").next().unwrap().to_string());
        output.insert(MetadataKey::Artist, reader.get_tag("artist").next().unwrap().to_string());
        output.insert(MetadataKey::Uri, uri.clone());

        output
    }

    fn service_type(&self) -> Service {
        Service::Local
    }

    fn prefered_sample_format(&self) -> Option<SampleFormat> {
        None
    }

    fn prefered_sample_rate(&self) -> Option<u32> {
        None
    }

    fn search(&self, query: &SearchQuery) -> Vec<String> {
        match self.list(&query.identifier) {
            Ok(result) => {
                let debug = result.clone();
                for a in debug {
                    println!("{:?}", a);
                }
                result
            }
            Err(error) => {
                warn!("Local file service query failed! ({:?})", error);
                Vec::<String>::new()
            }
        }
    }
}
