use proc_macro2::TokenStream;
use syn;

pub fn impl_to_json(ast: &syn::DeriveInput) -> TokenStream {
    let name = &ast.ident;
    quote! {
        impl ToJson for #name {
            fn to_json(&self) -> String {
                let result = serde_json::to_string(self);

                match result {
                    Ok(json) => json,
                    Err(err) => {
                        error!("JSON serialize failed: {}", err);
                        String::new()
                    }
                }
            }
        }
    }
}

pub fn impl_from_json(ast: &syn::DeriveInput) -> TokenStream {
    let name = &ast.ident;
    quote! {
        impl FromJson for #name {
            fn from_json_str<'a, T>(s: &'a str) -> T where T: Deserialize<'a> + Default,
            {
                let result = serde_json::from_str(s);
                match result {
                    Ok(instance) => instance,
                    Err(err) => {
                        error!("JSON deserialize failed: {}", err);
                        Default::default()
                    }
                }
            }

            fn from_json_string<'a, T>(s: &'a String) -> T where T: Deserialize<'a> + Default,
            {
                let result = serde_json::from_str(s.as_str());
                match result {
                    Ok(instance) => instance,
                    Err(err) => {
                        error!("JSON deserialize failed: {}", err);
                        Default::default()
                    }
                }
            }
        }
    }
}
