# This project is not active anymore and only acts as an archive. The idea lives on in another Project that will follow soon after I intergrated all lessons from my first Rust project "Tanzbox".


## ~~Tanzbox~~
-- The music player for your home sound system written in Rust --

Our goal is it to design a system simlar to other popular "All you can Play" Devices out there that cost a lot of money, but better.
Tanzbox is at heart a music player server that can be controlled on your local network over with a mobile friendly [BUI](https://en.wikipedia.org/wiki/Browser_user_interface)

Because it's 2018 an we mostly stream music these days we focus on implementing as many streaming services as possible like for example Spotify or Soundcloud. (That doesn't mean you don't need a premium subscription for some streaming services)
We achive this by integrating libraries like [librespot](https://github.com/librespot-org/librespot) or [youtube-dl](https://rg3.github.io/youtube-dl/)

Local files will also be supported at an later point for the music you payed hard cash for. 
All of this will be supported with a searching system and a playlist system that let's you mix and match between different services and streams. Want to listen to the complete Summertime '06 Album from Vince Staples? We got you covered!

At the moment we support the following services:
* Spotify
* Soundcloud (WIP)

... and more to come!

Even though the main application provides a client the server is client agnostic. So you could write your own desktop client for example if you would want to.

This project also focuses at the moment at the Raspberry Pi as it's main platform. That doesn't mean we won't support other systems and operating systems in the future. We try to keep things as platform independent as possible.

## Building / Install

Also note that this project is at it's infancy so there is no definite build guide or install guide yet.