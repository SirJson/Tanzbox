use cpal::Device;
pub use cpal::Format as AudioFormat;
pub use cpal::SampleFormat;
pub use cpal::SampleRate;
pub use cpal::StreamId;
use playback::BackendLoop;
use rb::{self, RbInspector, RB};
use std::collections::HashMap;
use std::fmt;
use std::sync::{Arc, RwLock};

pub type SharedStreamMap = Arc<RwLock<StreamMap>>;
type AudioStreamMap = HashMap<StreamId, AudioStreamData>;

#[derive(Debug)]
pub enum StreamError {
    UnknownStream,
    NotEnoughSamples,
}

#[derive(Debug)]
pub struct StreamMap {
    data: AudioStreamMap,
}

impl StreamMap {
    pub fn new() -> StreamMap {
        StreamMap { data: HashMap::new() }
    }

    pub fn shared() -> SharedStreamMap {
        Arc::new(RwLock::new(Self::new()))
    }

    pub fn spawn(
        &mut self,
        device: &Device,
        event_loop: BackendLoop,
        format: SampleFormat,
        sample_rate: u32,
        channels: u16,
        track_length: u64,

    ) -> StreamId {
        info!("Creating audio stream with sample rate '{}'...", sample_rate);

        let audio_format = AudioFormat {
            channels: channels,
            sample_rate: SampleRate(sample_rate),
            data_type: format,
        };

        debug!("Using Format {:?}", audio_format);

        let stream_id = event_loop
            .as_ref()
            .build_output_stream(device, &audio_format)
            .expect("Failed to create output stream");

        debug!("New stream id is '{:?}'", &stream_id);

        let (buffer, consumer, producer) = self.new_buffer(&audio_format, track_length);

        let stream = AudioStreamData {
            format: audio_format,
            id: stream_id.clone(),
            producer: producer,
            consumer: consumer,
            buffer: buffer,

        };

        self.data.insert(stream_id.clone(), stream);

        stream_id.clone()
    }

    pub fn contains(&self, id: &StreamId) -> bool {
        self.data.contains_key(&id)
    }

    pub fn audio_format(&self, id: &StreamId) -> Option<AudioFormat> {
        //TODO: Error Handling?
        match self.data.get(id) {
            Some(item) => Some(item.format.clone()),
            None => None,
        }
    }

    pub fn consumer(&self, id: &StreamId) -> Result<&AudioConsumer, StreamError> {
        if let Some(stream) = self.data.get(id) {
            let size = *&stream.buffer.count();
            if size < stream.format.sample_rate.0 as usize {
                return Err(StreamError::NotEnoughSamples);
            } else {
                return Ok(&stream.consumer);
            }
        } else {
            return Err(StreamError::UnknownStream);
        }
    }

    pub fn producer(&self, id: &StreamId) -> Result<&AudioProducer, StreamError> {
        if let Some(stream) = self.data.get(id) {
            Ok(&stream.producer)
        } else {
            Err(StreamError::UnknownStream)
        }
    }

    fn new_buffer(&self, format: &AudioFormat, track_length: u64) -> (AudioBuffer, AudioConsumer, AudioProducer) {
        let size = (format.sample_rate.0 as u64 * format.channels as u64 * track_length) as usize;
        debug!("Creating new Buffer with size {}", size);

        match format.data_type {
            SampleFormat::F32 => {
                let buf = rb::SpscRb::new(size);
                let consumer = AudioConsumer::Float(buf.consumer());
                let producer = AudioProducer::Float(buf.producer());
                (AudioBuffer::Float(buf), consumer, producer)
            }
            SampleFormat::I16 => {
                let buf: rb::SpscRb<i16> = rb::SpscRb::new(size);
                let consumer = AudioConsumer::Signed(buf.consumer());
                let producer = AudioProducer::Signed(buf.producer());
                (AudioBuffer::Signed(buf), consumer, producer)
            }
            SampleFormat::U16 => unimplemented!(),
        }
    }
}
#[allow(dead_code)]
pub enum AudioConsumer {
    Float(rb::Consumer<f32>),
    Signed(rb::Consumer<i16>),
    Unsigned(rb::Consumer<u16>),
}

impl fmt::Debug for AudioConsumer {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "AudioConsumer::?")
    }
}

#[allow(dead_code)]
pub enum AudioProducer {
    Float(rb::Producer<f32>),
    Signed(rb::Producer<i16>),
    Unsigned(rb::Producer<u16>),
}

impl fmt::Debug for AudioProducer {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "AudioProducer::?")
    }
}

#[allow(dead_code)]
pub enum AudioBuffer {
    Float(rb::SpscRb<f32>),
    Signed(rb::SpscRb<i16>),
    Unsigned(rb::SpscRb<u16>),
}

impl fmt::Debug for AudioBuffer {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "AudioBuffer::?")
    }
}

impl rb::RbInspector for AudioBuffer {
    fn is_empty(&self) -> bool {
        match &self {
            &AudioBuffer::Float(buf) => buf.is_empty(),
            &AudioBuffer::Signed(buf) => buf.is_empty(),
            &AudioBuffer::Unsigned(buf) => buf.is_empty(),
        }
    }
    fn is_full(&self) -> bool {
        return !self.is_empty();
    }
    fn capacity(&self) -> usize {
        match &self {
            &AudioBuffer::Float(buf) => buf.capacity(),
            &AudioBuffer::Signed(buf) => buf.capacity(),
            &AudioBuffer::Unsigned(buf) => buf.capacity(),
        }
    }
    fn slots_free(&self) -> usize {
        match &self {
            &AudioBuffer::Float(buf) => buf.slots_free(),
            &AudioBuffer::Signed(buf) => buf.slots_free(),
            &AudioBuffer::Unsigned(buf) => buf.slots_free(),
        }
    }
    fn count(&self) -> usize {
        match &self {
            &AudioBuffer::Float(buf) => buf.count(),
            &AudioBuffer::Signed(buf) => buf.count(),
            &AudioBuffer::Unsigned(buf) => buf.count(),
        }
    }
}

#[derive(Debug)]
pub struct AudioStreamData {
    pub format: AudioFormat,
    pub id: StreamId,
    consumer: AudioConsumer,
    producer: AudioProducer,
    buffer: AudioBuffer,

}
