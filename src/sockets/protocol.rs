use playback::{Service};
use std::collections::HashMap;
use derives::ToJson;
use serde_json;

#[derive(Serialize, Deserialize, Clone, Debug, Default)]
pub struct TrackRequest {
    pub service: Service,
    pub uri: String
}

#[derive(Serialize, Deserialize, Clone, ToJson, Debug, Default)]
pub struct SearchQuery
{
    pub identifier: String,
    pub service: Service,
    pub arguments: Option<HashMap<String,String>>
}

#[derive(Serialize, Deserialize, Clone, ToJson, Debug, Default)]
pub struct PositionUpdate
{
    pub elapsed: u32,
    pub length: u32
}

build_protocol!([client, version: 0x02] =>
                { (Success, 0x01) => [] },
                { (Fail, 0x02) => [] },
                { (TrackStatusUpdate, 0x03) => [(payload: ::playback::PlayerStatus)] },
                { (TrackPositionUpdate, 0x04) => [(payload: ::sockets::protocol::PositionUpdate)] },
                { (PlayTrack, 0x05) => [(payload: ::uuid::Uuid)] },
                { (QueueDirty, 0x06) => [(payload: ::playback::PlayQueue)] }, 
                { (Queue, 0x07) => [(payload: String)] },
                { (SearchResult, 0x08) => [(payload: Vec<::playback::Metadata>)] },
                { (PlayerState, 0x09) => [(payload: ::playback::PlayerInfo)] });

build_protocol!([server, version: 0x02] =>
                { (QueuePlay, 0x01) => [(payload: ::uuid::Uuid)] },
                { (QueueRemove, 0x02) => [(payload: ::uuid::Uuid)] },
                { (ResumePlay, 0x03) => [] },
                { (PausePlay, 0x04) => [] },
                { (QueueClear, 0x05) => [] },
                { (SkipTrack, 0x06) => [] },
                { (GetQueue, 0x07) => [] },
                { (GetPlayerState, 0x08) => [] },
                { (AddTrack, 0x09) => [(payload: ::sockets::protocol::TrackRequest)] },
                { (SearchTrack, 0x0A) => [(payload: ::sockets::protocol::SearchQuery)] });
