use librespot::metadata::Track as SpotifyTrackMetadata;
use prelude::AsTypeOrDefault;
use prelude::ToJson;
use serde_json;
use std::collections::HashMap;
use std::convert::From;

pub type Metadata = HashMap<MetadataKey, String>;

#[derive(Serialize, Deserialize, Clone, Eq, PartialEq, Hash, Debug)]
pub enum MetadataKey {
    Duration,
    Title,
    Artist,
    Album,
    Cover,
    Uri,
}

pub trait MetadataReader {
    fn get_number(&self, key: MetadataKey) -> Option<u64>;
    fn get_string(&self, key: MetadataKey) -> Option<String>;
}

pub trait MetadataExt {
    fn default() -> Metadata;
}

pub trait FromYoutubeDl {
    fn from_youtube_dl(json: serde_json::Value) -> Vec<Metadata>;
}

pub trait FromSpotify {
    fn from_spotify(data: &SpotifyTrackMetadata) -> Metadata;
}

impl ToJson for Metadata {
    fn to_json(&self) -> String {
        let result = serde_json::to_string(self);

        match result {
            Ok(json) => json,
            Err(err) => {
                error!("JSON serialize failed: {}", err);
                String::new()
            }
        }
    }
}

impl FromYoutubeDl for Metadata {
    fn from_youtube_dl(json: serde_json::Value) -> Vec<Metadata> {
        let data = json.as_array().unwrap();
        data.into_iter().map(|x| parse_youtube_dl_json(&x)).collect()
    }
}

fn parse_youtube_dl_json(json: &serde_json::Value) -> Metadata {
    let mut output = Metadata::new();

    output.insert(MetadataKey::Duration, json["duration"].as_u64_or_default().to_string());
    output.insert(MetadataKey::Title, json["title"].as_string_or_default());
    output.insert(MetadataKey::Artist, json["uploader"].as_string_or_default());
    output.insert(MetadataKey::Cover, json["thumbnail"].as_string_or_default());
    output.insert(MetadataKey::Uri, json["webpage_url"].as_string_or_default());

    output
}

impl FromSpotify for Metadata {
    fn from_spotify(data: &SpotifyTrackMetadata) -> Metadata {
        let mut output = Metadata::new();

        output.insert(MetadataKey::Duration, ((data.duration / 1000) as u64).to_string());
        output.insert(MetadataKey::Title, data.name.clone());
        output.insert(MetadataKey::Uri, String::default());

        output
    }
}

impl MetadataExt for Metadata {
    fn default() -> Metadata {
        let mut output = Metadata::new();
        output.insert(MetadataKey::Title, String::from("UNKNOWN"));
        output.insert(MetadataKey::Duration, String::from("0"));
        output.insert(MetadataKey::Uri, String::from("?"));

        output
    }
}

impl MetadataReader for Metadata {
    fn get_number(&self, key: MetadataKey) -> Option<u64> {
        match self.get(&key) {
            Some(num) => num.parse().ok(),
            None => {
                debug!("Tried to access metadata number \"{:?}\" but it could not be found", key);
                None
            }
        }
    }

    fn get_string(&self, key: MetadataKey) -> Option<String> {
        match self.get(&key) {
            Some(&ref str_val) => Some(str_val.clone()),
            None => {
                debug!("Tried to access metadata string \"{:?}\" but it could not be found", key);
                None
            }
        }
    }
}
