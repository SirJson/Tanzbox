#!/usr/bin/env python3

import subprocess
import sys
import traceback
from pathlib import Path

CURRENT_DIR = Path.cwd()
FRONTEND_DIR = CURRENT_DIR / 'client'

def start_frontend():
    dir_str = str(FRONTEND_DIR)
    cmd = f'gnome-terminal -- bash -c "cd {dir_str}; ng serve; read"'
    subprocess.Popen(cmd, shell=True, stdin=None, stdout=None, stderr=None, close_fds=True)


def frontend_running():
    return subprocess.call(['nc', '-z', 'localhost', '4200']) == 0

def main():
    try:
        if not frontend_running():
            print("Start Frontend")
            start_frontend()
        else:
            print("Frontend already running")
        print("Start server..")
        subprocess.run(['cargo', 'run'], cwd=CURRENT_DIR)
    except KeyboardInterrupt:
        print("Shutdown requested...exiting")
    except Exception:
        traceback.print_exc(file=sys.stdout)
    sys.exit(0)

if __name__ == "__main__":
    main()
