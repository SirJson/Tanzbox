use playback::{Metadata, PlayerInfo};
use prelude::*;
use serde_json;
use sockets::protocol::{client, server};
use ws::Message as NetMessage;
use ws::{CloseCode, Error, Handler, Handshake, Result, Sender};

pub struct ServerHandler {
    sender: Sender,
    player_channel: EventSender,
}

impl ServerHandler {
    pub fn new(snd: Sender, player_tx: EventSender) -> ServerHandler {
        trace!("Creating ServerHandler...");
        ServerHandler {
            sender: snd,
            player_channel: player_tx,
        }
    }

    fn try_send(&self, data: String) {
        match self.sender.send(data) {
            Ok(_) => (),
            Err(error) => {
                warn!("Failed to send message: {}", error);
            }
        }
    }
}

impl Handler for ServerHandler {
    fn on_open(&mut self, shake: Handshake) -> Result<()> {
        let remote_address = shake.remote_addr();
        if remote_address.is_err() {
            warn!("Incomming client connection from INVALID address");
        } else {
            match remote_address.unwrap() {
                Some(addr) => info!("Incoming client connection from address: \"{}\"", addr),
                None => warn!("Incoming client connection from UNKNOWN address"),
            }
        }
        Ok(())
    }

    fn on_message(&mut self, msg: NetMessage) -> Result<()> {
        let data: serde_json::Value = match msg.into_text() {
            Ok(text) => match serde_json::from_str(text.as_str()) {
                Ok(value) => value,
                Err(error) => {
                    warn!("Failed to parse JSON data from WebSocket: {:?}", error);
                    return Ok(());
                }
            },
            Err(error) => {
                warn!("Failed to read from WebSocket: {:?}", error);
                return Ok(());
            }
        };

        if !server::valid_header(&data) {
            warn!("Packet droped! Invalid header!");
            return Ok(());
        }

        let cmd = server::command(&data);
        debug!("Received command: {:?}", &cmd);
        match cmd {
            server::Command::QueuePlay => {
                let message = server::QueuePlay::from_json_value(data);
                self.player_channel.send(Event::PlayQueueStart(message.payload))
            }
            server::Command::QueueRemove => {
                unimplemented!()
                /*
                let message = server::QueueRemove::from_json_value(data);
                self.player_channel.send(Event::PlayQueueRemove(message.index))
                */
            }
            server::Command::ResumePlay => {
                self.player_channel.send(Event::ResumeTrack);
            }
            server::Command::PausePlay => {
                self.player_channel.send(Event::PauseTrack);
            }
            server::Command::QueueClear => {
                self.player_channel.send(Event::PlayQueueClear);
            }
            server::Command::SkipTrack => {
                self.player_channel.send(Event::SkipTrack);
            }
            server::Command::GetPlayerState => {
                let (tx, rx) = temp_channel::<PlayerInfo>();
                self.player_channel.send(Event::RequestPlayerInfo(tx));
                let state = rx.recv().unwrap_or_default();
                self.try_send(client::PlayerState::new(state).to_json());
            }
            server::Command::SearchTrack => {
                let (tx, rx) = temp_channel::<Vec<Metadata>>();
                let search = server::SearchTrack::from_json_value(data);
                self.player_channel.send(Event::Search(search.payload, tx));
                let result = rx.recv().unwrap_or_default();
                debug!("send result");
                self.try_send(client::SearchResult::new(result).to_json());
            }
            server::Command::AddTrack => {
                /*
                let (tx, rx) = temp_channel::<String>();
                let search = server::SearchTrack::from_json_value(data);
                self.player_channel.send(Event::Search(search.payload, tx));
                let result = rx.recv().unwrap_or_default();
                self.try_send(client::SearchResult::new(result).to_json());
                */
                let request = server::AddTrack::from_json_value(data);
                debug!("Track Request: {:?}", request.payload);
                self.player_channel.send(Event::PlayQueueAdd(request.payload));
            }
            _ => warn!("Invalid command: {:?}", cmd.clone()),
        }

        Ok(())
    }

    /// Called any time this endpoint receives a close control frame.
    /// This may be because the other endpoint is initiating a closing handshake,
    /// or it may be the other endpoint confirming the handshake initiated by this endpoint.
    fn on_close(&mut self, code: CloseCode, reason: &str) {
        match code {
            CloseCode::Normal => info!("A client is done with the connection."),
            CloseCode::Away => info!("A client is leaving the site."),
            CloseCode::Abnormal => error!("Client closing handshake failed! Unable to obtain closing status from client."),
            _ => error!("A client encountered an error: {}", reason),
        }
    }

    /// Called when an error occurs on the WebSocket.
    fn on_error(&mut self, err: Error) {
        error!("A client encountered an error: {:?}", err);
    }
}
