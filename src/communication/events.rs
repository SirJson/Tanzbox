use crossbeam_channel as message;
use futures::sync::mpsc as task_mpsc;
use playback::streams::{SharedStreamMap, StreamId};
use playback::{Metadata, PlayQueue, PlayerInfo};
use sockets::protocol::{SearchQuery, TrackRequest};
use std::default::Default;
use std::fmt::{self, Display, Formatter};
use std::time::Duration;
use uuid;

pub trait RaiseEvent {
    fn raise(&self, code: Event);
}

impl RaiseEvent for task_mpsc::UnboundedSender<Event> {
    fn raise(&self, code: Event) {
        self.unbounded_send(code).unwrap_or(());
    }
}

#[derive(Debug, Clone, AsRefStr)]
pub enum Event {
    Null,
    Halt,
    Shutdown,
    PlayTrack(uuid::Uuid),
    PauseTrack,
    StopTrack,
    ResumeTrack,
    SkipTrack,
    PlayQueueStart(uuid::Uuid),
    PlayQueueAdd(TrackRequest),
    PlayQueueRemove(u32),
    PlayQueueClear,
    RequestPlayQueue(message::Sender<PlayQueue>),
    RequestPlayerInfo(message::Sender<PlayerInfo>),
    RequestSoundcloudMeta(message::Sender<String>, String),
    TrackStateUpdate { left: Duration, initial: Duration, elapsed: Duration },
    TrackFinished,
    QueueModified(PlayQueue),
    MetadataUpdate(Metadata),
    LoadTrack(String),
    Search(SearchQuery, message::Sender<Vec<Metadata>>),
    StreamCreated { id: StreamId, streams: SharedStreamMap },
    PCMF32Data(Vec<f32>)
}

impl Display for Event {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{}", self.as_ref())
    }
}

impl Default for Event {
    fn default() -> Event {
        Event::Null
    }
}
