use librespot::playback::audio_backend::{Open, Sink};
use std::io;

pub struct InternalSink {}

impl InternalSink {}

impl Open for InternalSink {
    fn open(option: Option<String>) -> InternalSink {
        debug!("Open internal Spotify sink");
        for opt in option {
            debug!("\t=> {}", opt);
        }

        InternalSink {}
    }
}

impl Sink for InternalSink {
    fn start(&mut self) -> io::Result<()> {
        debug!("Start Spotify sink!");
        Ok(())
    }

    fn stop(&mut self) -> io::Result<()> {
        debug!("Stop Spotify sink!");
        Ok(())
    }

    fn write(&mut self, _data: &[i16]) -> io::Result<()> {
        Ok(())
    }
}
