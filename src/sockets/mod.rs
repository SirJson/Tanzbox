#[macro_use]
pub mod protocol_builder;
mod controller;
pub mod protocol;
pub mod server;

use self::controller::Controller;
pub use self::protocol::*;
use config;
use prelude::*;
use ws;

fn create<F, H>(factory: F) -> Result<ws::WebSocket<F>, ws::Error>
where
    F: FnMut(ws::Sender) -> H,
    H: ws::Handler,
{
    let ws = ws::WebSocket::new(factory)?;
    Ok(ws)
}

pub fn start(config: &config::WebSockets) -> ThreadHandle {
    let channels = channel_table();
    let player_channel = channels.sender(Channel::Player);
    let controller_channel = channels.receiver(Channel::WebSockets);
    let ws_cfg = config.clone();
    match create(move |out| server::ServerHandler::new(out, player_channel.clone())) {
        Ok(socket) => {
            let broadcaster = socket.broadcaster();
            named_thread("websocket_controller", move || {
                Controller::new(broadcaster, controller_channel).run();
            });
            named_thread("websocket_handler", move || {
                if let Err(listen_err) = socket.listen(ws_cfg.address.clone()) {
                    panic!("Failed to listen on address \"{}\". Reason: {:?}", ws_cfg.address, listen_err);
                }
                info!("WebSocket Handler shutdown!");
            })
        }
        Err(error) => {
            panic!("Failed to initialize WebSocket Server: {:?}", error);
        }
    }
}
