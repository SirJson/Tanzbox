use super::sink::InternalSink;
use config;
use futures::sync::mpsc::UnboundedReceiver;
use futures::{Async, Future, Poll, Stream};
use librespot::core::authentication::Credentials;
use librespot::core::config::SessionConfig;
use librespot::core::session::Session;
use librespot::core::spotify_id::SpotifyId;
use librespot::playback::audio_backend::{Open, Sink};
use librespot::playback::config::{Bitrate, PlayerConfig};
use librespot::playback::player::Player as SpotifyPlayer;
use librespot::playback::player::PlayerEvent;
use prelude::*;
use std::fmt;
use std::io::Error as IOError;
use std::sync::{Arc, Barrier};
use tokio_core::reactor;

type SpotifyEventReceiver = UnboundedReceiver<PlayerEvent>;

bitflags! {
    struct PollingTask: u32 {
        const NOTHING = 0b00000001;
        const SESSION_CONNECT = 0b00000010;
        const LOADING_TRACK = 0b00000100;
        const STARTING_TRACK = 0b00001000;
    }
}

impl Default for PollingTask {
    fn default() -> PollingTask {
        PollingTask::NOTHING
    }
}

struct TaskData {
    loading_track_id: Option<SpotifyId>,
}

impl Default for TaskData {
    fn default() -> TaskData {
        TaskData { loading_track_id: None }
    }
}

pub struct SpotifyObjects {
    session: Option<Session>,
    player: Option<SpotifyPlayer>,
    event_receiver: Option<SpotifyEventReceiver>,
}

impl fmt::Display for SpotifyObjects {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let session_state = if self.session.is_some() { "SET" } else { "UNDEFINED" };
        let player_state = if self.session.is_some() { "SET" } else { "UNDEFINED" };
        let event_state = if self.event_receiver.is_some() { "SET" } else { "UNDEFINED" };

        write!(
            f,
            "SpotifyObjects => [session = {}, player = {}, event_rx = {}]",
            session_state, player_state, event_state
        )
    }
}

pub struct SpotifyTasks {
    session_connect: Option<Box<Future<Item = Session, Error = IOError>>>,
    data: TaskData,
}

impl Default for SpotifyObjects {
    fn default() -> SpotifyObjects {
        SpotifyObjects {
            session: None,
            player: None,
            event_receiver: None,
        }
    }
}

impl Default for SpotifyTasks {
    fn default() -> SpotifyTasks {
        SpotifyTasks {
            session_connect: None,
            data: TaskData::default(),
        }
    }
}

// Daemon because it's always running and waiting for instructions
pub struct DecoderDaemon {
    spotify: SpotifyObjects,
    tasks: SpotifyTasks,
    event_loop_handle: reactor::Handle,
    event_receiver: TaskEventReceiver,

    active_tasks: PollingTask,
    todo: PollingTask,
    credentials: Credentials,
    running: bool,
}

impl DecoderDaemon {
    pub fn start(config: config::Spotify,  event_rx: TaskEventReceiver) -> ThreadHandle {
        let credentials = Credentials::with_password(config.username, config.password);

        debug!("Initializing Spotify decoder...");

        named_thread("spotify_decoder", move || {
            let mut core = reactor::Core::new().unwrap();
            let handle = core.handle();
            core.run(DecoderDaemon::init(credentials, handle, event_rx)).unwrap();
            info!("Spotify decoder shutdown!");
        })
    }

    pub fn init(creds: Credentials, evt_handle: reactor::Handle, event_rx: TaskEventReceiver) -> DecoderDaemon {
        DecoderDaemon {
            spotify: SpotifyObjects::default(),
            tasks: SpotifyTasks::default(),
            event_loop_handle: evt_handle,
            event_receiver: event_rx,

            active_tasks: PollingTask::default(),
            todo: PollingTask::default(),
            credentials: creds,
            running: true,
        }
    }

    fn poll_spotify_events(&mut self) -> Option<PlayerEvent> {
        if self.spotify.event_receiver.is_none() {
            return None;
        }
        let result = self.spotify.event_receiver.as_mut().unwrap().poll();
        if result.is_err() {
            error!("Spotify Event polling failed: {:?}", result.unwrap_err());
            return None;
        }
        if let Async::Ready(answer) = result.unwrap() {
            if let Some(event) = answer {
                trace!("Received internal Spotify Event: {:?}", event);
                return Some(event);
            }
        }
        None
    }

    fn poll_events(&mut self) -> Event {
        let result = self.event_receiver.poll();
        if result.is_err() {
            error!("Event polling failed: {:?}", result.unwrap_err());
            return Event::Null;
        }
        if let Async::Ready(answer) = result.unwrap() {
            if let Some(event) = answer {
                return event;
            }
        }
        Event::Null
    }

    fn handle_events(&mut self) {
        let event = self.poll_events();

        trace_event!(event);

        match event {
            Event::Shutdown => self.running = false,
            Event::LoadTrack(uri) => self.load_track(uri),
            Event::StreamCreated { id, streams } => self.play_track(),
            Event::StopTrack => self.stop_track(),
            _ => (),
        }
    }

    fn stop_track(&self) {
        self.spotify.player.as_ref().unwrap().stop();
    }

    fn play_track(&self) {
        self.spotify.player.as_ref().unwrap().play();
    }

    fn load_track(&mut self, uri: String) {
        match SpotifyId::from_base62(uri.as_str()) {
            Ok(track_id) => {
                debug!("Loading spotify:track:\"{}\"", uri);
                self.tasks.data.loading_track_id = Some(track_id);
                self.add_polling_task(PollingTask::LOADING_TRACK);
            }
            Err(error) => {
                error!("spotify:track:\"{}\" is invalid! Reason: {:?}", uri, error);
            }
        }
    }

    fn task_todo(&self, task_flag: PollingTask) -> bool {
        return self.todo.contains(task_flag);
    }

    fn create_player(&mut self, session: Session) -> SpotifyPlayer {
        let player_config = PlayerConfig {
            bitrate: Bitrate::Bitrate320,
            normalisation: true,
            normalisation_pregain: 0.0,
        };

        let backend = mk_internal_sink::<InternalSink>;
        let (player, player_event) = SpotifyPlayer::new(player_config, session, None, move || (backend)(None));

        self.spotify.event_receiver = Some(player_event);

        player
    }

    fn add_polling_task(&mut self, task_flag: PollingTask) {
        match task_flag {
            PollingTask::SESSION_CONNECT => {
                self.tasks.session_connect = Some(Session::connect(
                    SessionConfig::default(),
                    self.credentials.clone(),
                    None,
                    self.event_loop_handle.clone(),
                ));
            }
            PollingTask::LOADING_TRACK => {
                if self.spotify.player.is_none() {
                    error!("No Spotify playback availiable.");
                    return;
                } else if self.spotify.event_receiver.is_none() {
                    error!("No Spotify Event receiver availiable.");
                    return;
                } else if self.tasks.data.loading_track_id.is_none() {
                    error!("No loading track id availiable.");
                    return;
                }
                self.spotify
                    .player
                    .as_mut()
                    .unwrap()
                    .load(self.tasks.data.loading_track_id.unwrap(), false, 0)
                    .poll()
                    .unwrap();
            }
            _ => {
                warn!("Unknown task: {:?}", task_flag);
                return;
            }
        }
        self.active_tasks.insert(task_flag);
        self.todo.insert(task_flag);
    }

    fn schedule_tasks(&mut self) {
        self.todo = self.active_tasks;

        if self.spotify.session.is_none() && self.tasks.session_connect.is_none() {
            debug!("Session connect scheduled");
            self.add_polling_task(PollingTask::SESSION_CONNECT);
        }
    }

    fn resolve_tasks(&mut self, last_spotify_event: Option<PlayerEvent>) {
        if self.task_todo(PollingTask::SESSION_CONNECT) {
            if let Ok(Async::Ready(new_session)) = self.tasks.session_connect.as_mut().unwrap().poll() {
                let session = new_session;
                info!("Spotify Login successful");
                self.active_tasks.remove(PollingTask::SESSION_CONNECT);
                self.spotify.player = Some(self.create_player(session.clone()));
                self.spotify.session = Some(session);
                self.tasks.session_connect = None;
            }
            self.todo.remove(PollingTask::SESSION_CONNECT);
        }

        if self.task_todo(PollingTask::LOADING_TRACK) {
            trace!("Loading track...");
            if let Some(event) = last_spotify_event {
                match event {
                    PlayerEvent::Stopped { track_id } => {
                        let track_request = self.tasks.data.loading_track_id.unwrap();
                        if track_id != track_request {
                            // TODO: Better error handling
                            panic!("Spotify loaded the wrong track")
                        }
                        info!("Track is ready");
                        self.tasks.data.loading_track_id = None;
                        self.active_tasks.remove(PollingTask::LOADING_TRACK);

                    }
                    _ => (),
                }
            }
            self.todo.remove(PollingTask::LOADING_TRACK);
        }
    }
}

impl Future for DecoderDaemon {
    type Item = ();
    type Error = ();

    fn poll(&mut self) -> Poll<(), ()> {
        trace!("Spotify decoder polled");
        loop {
            let active_tasks_start = self.active_tasks.clone();

            self.handle_events();
            self.schedule_tasks();
            let event = self.poll_spotify_events();
            self.resolve_tasks(event);

            let active_tasks_end = self.active_tasks.clone();

            if active_tasks_start == active_tasks_end {
                break;
            }
        }

        if self.running {
            return Ok(Async::NotReady);
        } else {
            debug!("Spotify decoder shutdown!");
            return Ok(Async::Ready(()));
        }
    }
}

fn mk_internal_sink<S: Sink + Open + 'static>(device: Option<String>) -> Box<Sink> {
    let sink = InternalSink::open(device);
    Box::new(sink)
}
