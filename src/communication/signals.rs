use std::sync::{Arc, Condvar, Mutex};

#[derive(Clone)]
pub struct Signal {
    payload: Arc<(Mutex<bool>, Condvar)>,
}

impl Signal {
    pub fn new() -> Signal {
        Signal {
            payload: Arc::new((Mutex::new(false), Condvar::new())),
        }
    }

    pub fn wait(self) {
        let &(ref lock, ref cvar) = &*self.payload;
        let mut fired = lock.lock().unwrap();
        while !*fired {
            fired = cvar.wait(fired).unwrap();
        }
    }

    pub fn notify(self) {
        let &(ref lock, ref cvar) = &*self.payload;
        let mut fired = lock.lock().unwrap();
        *fired = true;
        cvar.notify_all();
    }
}

pub fn signal_pair() -> (Signal, Signal)
{
    let a = Signal::new();
    let b = a.clone();
    (a, b)
}