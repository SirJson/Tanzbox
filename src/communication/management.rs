use super::events::Event;
use crossbeam_channel as message;
use futures::sync::mpsc::{unbounded, UnboundedReceiver, UnboundedSender};
use std::collections::HashMap;
use strum::EnumProperty;
use strum::IntoEnumIterator;

static mut CHANNEL_TABLE_INSTANCE: Option<ChannelTable> = None;
const TYPE_ASYNC: &'static str = "async";
const TYPE_SYNC: &'static str = "sync";

pub fn channel_table<'a>() -> &'a ChannelTable {
    unsafe {
        match CHANNEL_TABLE_INSTANCE {
            Some(ref broker) => broker,
            None => {
                debug!("Initializing channel table...");
                CHANNEL_TABLE_INSTANCE = Some(ChannelTable::build());
                CHANNEL_TABLE_INSTANCE.as_ref().unwrap()
            }
        }
    }
}

//TODO: Do we REALLY need strum?
#[derive(Debug, PartialEq, Eq, Hash, Clone, EnumIter, EnumProperty, Display, Copy)]
pub enum Channel {
    #[strum(props(Type = "async"))]
    Kernel,
    #[strum(props(Type = "async"))]
    Player,
    #[strum(props(Type = "sync"))]
    PlaybackScheduler,
    #[strum(props(Type = "async"))]
    AudioDecoder,
    #[strum(props(Type = "async"))]
    AudioProcessor,
    #[strum(props(Type = "async"))]
    AudioWorker,
    #[strum(props(Type = "async"))]
    SpotifyService,
    #[strum(props(Type = "async"))]
    SpotifySink,
    #[strum(props(Type = "async"))]
    WebSockets,
    #[strum(props(Type = "async"))]
    SystemSignal,
    #[strum(props(Type = "async"))]
    Streams
}

pub struct ChannelTable {
    sender: HashMap<Channel, message::Sender<Event>>,
    receiver: HashMap<Channel, message::Receiver<Event>>,
}

pub fn event_channel() -> (message::Sender<Event>, message::Receiver<Event>) {
    message::unbounded::<Event>()
}

pub fn future_event_channel() -> (UnboundedSender<Event>, UnboundedReceiver<Event>) {
    unbounded::<Event>()
}

pub fn bounded_event_channel(cap: usize) -> (message::Sender<Event>, message::Receiver<Event>) {
    message::bounded::<Event>(cap)
}

pub fn temp_channel<T>() -> (message::Sender<T>, message::Receiver<T>) {
    message::unbounded::<T>()
}

impl ChannelTable {
    pub fn build() -> ChannelTable {
        let mut senders: HashMap<Channel, message::Sender<Event>> = HashMap::new();
        let mut receivers: HashMap<Channel, message::Receiver<Event>> = HashMap::new();

        for channel in Channel::iter() {
            let channel_type = channel.get_str("Type").expect("Channel is missing type");
            debug!("Creating {} channel '{:?}'", channel_type, channel);
            match channel_type {
                TYPE_ASYNC | TYPE_SYNC => {
                    let result = if channel_type == TYPE_ASYNC {
                        event_channel()
                    } else {
                        bounded_event_channel(1)
                    };
                    senders.insert(channel, result.0);
                    receivers.insert(channel, result.1);
                }
                _ => panic!("Unknown type '{}' defined for Channel '{}'", channel_type, channel),
            }
        }

        ChannelTable {
            sender: senders,
            receiver: receivers,
        }
    }

    pub fn broadcast(&self, code: Event) {
        for sender in self.sender.values() {
            sender.send(code.clone());
        }
    }

    pub fn sender(&self, index: Channel) -> message::Sender<Event> {
        self.sender[&index].clone()
    }

    pub fn receiver(&self, index: Channel) -> message::Receiver<Event> {
        self.receiver[&index].clone()
    }
}
