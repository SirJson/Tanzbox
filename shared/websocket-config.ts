
export interface WebSocketConfig
{
    frontend_address: string;
    spotify_controller_address: string;
}