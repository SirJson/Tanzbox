use config;
use playback::decoder::InetAudioDecoder;
use playback::services::AudioService;
use playback::streams::{AudioFormat, SampleFormat};
use playback::{Metadata, MetadataFromYoutubeDl, MetadataKey, Service};
use sockets::protocol::SearchQuery;

pub struct SoundCloud {
    decoder: InetAudioDecoder,
    next_uri: String,
    next_format: Option<AudioFormat>,
}

impl SoundCloud {
    pub fn new(config: config::Playback) -> SoundCloud {
        info!("Initializing SoundCloud service...");
        SoundCloud {
            decoder: InetAudioDecoder::open(config),
            next_uri: String::default(),
            next_format: None,
        }
    }
}

impl AudioService for SoundCloud {
    fn load(&mut self, uri: &String, format: &AudioFormat) {
        info!("Open SoundCloud stream \"{}\"...", &uri);
        self.next_uri = uri.clone();
        self.next_format = Some(format.clone());
    }

    fn play(&mut self, format: &AudioFormat) {
        let format = self.next_format.take().expect("Tried to play stream that was not loaded before");
        self.decoder.start(&self.next_uri, &format);
    }

    fn pause(&self) {
        debug!("Sending pause stream to audio processor...");
        //self.audio_processor.borrow().pause_stream();
    }

    fn resume(&self) {
        debug!("Sending resume stream to audio processor...");
        //self.audio_processor.borrow().play_stream();
    }

    fn stop(&self) {
        debug!("Sending stop stream to audio processor...");
        //self.audio_processor.pause_stream();
    }

    fn shutdown(&mut self) {
        debug!("Destroy stream!");
        //self.audio_processor.destroy_stream();
    }

    fn is_ready(&self) -> bool {
        true
    }

    fn get_metadata(&self, uri: &String) -> Metadata {
        let result = Metadata::from_youtube_dl(self.decoder.get_metadata(&uri));
        result[0].clone()
    }

    fn service_type(&self) -> Service {
        Service::SoundCloud
    }

    fn prefered_sample_format(&self) -> Option<SampleFormat> {
        None
    }

    fn prefered_sample_rate(&self) -> Option<u32> {
        None
    }

    fn search(&self, query: &SearchQuery) -> Vec<String> {
        let metadata = Metadata::from_youtube_dl(self.decoder.get_metadata(&query.identifier));
        metadata.into_iter().map(|item| item.get(&MetadataKey::Uri).unwrap().clone()).collect()
    }
}
