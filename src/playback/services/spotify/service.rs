use config;
use playback::services::spotify::decoder::DecoderDaemon;
use playback::services::AudioService;
use playback::streams::{AudioFormat, AudioProducer, SampleFormat};
use playback::{Metadata, MetadataKey, Service};
use prelude::*;
use sockets::protocol::SearchQuery;
use std::sync::{Arc, Barrier};

pub struct Spotify {
    ready: bool,
    decoder_events: TaskEventSender,
    sink_events: EventSender,
}

impl Spotify {
    pub fn new(config: config::Spotify) -> Spotify {
        debug!("Initializing Spotify service...");
        let channels = channel_table();
        let daemon_channel = future_event_channel();

        DecoderDaemon::start(config, daemon_channel.1);

        Spotify {
            decoder_events: daemon_channel.0,
            sink_events: channels.sender(Channel::SpotifySink),
            ready: true,
        }
    }
}
//TODO: Mailbox?
impl AudioService for Spotify {
    fn load(&mut self, uri: &String, _format: AudioFormat) {
        self.decoder_events.raise(Event::LoadTrack(uri.clone()));
    }

    fn play(&mut self, stream: &AudioProducer, format: &AudioFormat) {
        unimplemented!()
    }

    fn pause(&self) {}

    fn resume(&self) {}

    fn stop(&self) {}

    fn shutdown(&mut self) {}

    fn is_ready(&self) -> bool {
        self.ready
    }

    fn get_metadata(&self, _uri: &String) -> Metadata {
        let mut output = Metadata::new();

        output.insert(MetadataKey::Duration, 127.to_string());
        output.insert(MetadataKey::Title, "Burgundy".to_string());
        output.insert(MetadataKey::Artist, "Earl Sweatshirt".to_string());
        output.insert(MetadataKey::Album, "Doris".to_string());
        output.insert(
            MetadataKey::Cover,
            "https://i.scdn.co/image/69c5c546289cead3c93a421dff4f7708f7c4eff5".to_string(),
        );
        output.insert(MetadataKey::Uri, "43tVqovviXhkmOV5LOkTn7".to_string());

        output
    }

    fn service_type(&self) -> Service {
        Service::Spotify
    }

    fn prefered_sample_format(&self) -> Option<SampleFormat> {
        Some(SampleFormat::I16)
    }

    fn prefered_sample_rate(&self) -> Option<u32> {
        Some(44100)
    }

    fn search(&self, query: &SearchQuery) -> Vec<String> {
        vec![query.identifier.clone()]
    }
}
