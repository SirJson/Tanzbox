#!/usr/bin/env python3

import os
import subprocess
import distutils
import shutil
import datetime
import urllib.request
import argparse
import sys
from pathlib import Path
from distutils import dir_util
from zipfile import ZipFile, ZIP_DEFLATED

parser = argparse.ArgumentParser()
parser.add_argument('--target_dir', help='The directory where the finished package will be found')
parser.add_argument('--assemble_dir', help='The directory where the package will be assembled')
args = parser.parse_args()

client_src = 'client'
target_bin = 'target/armv7-unknown-linux-gnueabihf/release/tanzbox_server'
target_bin_debug = 'target/armv7-unknown-linux-gnueabihf/debug/tanzbox_server'
target_bin_path = None
target_dir = 'release/rpi'
assemble_dir = 'assemble/rpi'
config = 'etc/tanzbox-rpi3-example.toml'
build_time = datetime.datetime.now().strftime('%Y-%m-%d_%H_%M_%S')
package_name = f'tanzbox_rpi_{build_time}'
package_base_dir = 'tanzbox'

if args.target_dir: target_dir = args.target_dir
if args.assemble_dir: assemble_dir = args.assemble_dir

# Resolve all Paths.. just in case...

client_src = Path(client_src).resolve()
target_bin = Path(target_bin).resolve()
target_bin_debug = Path(target_bin_debug).resolve()
target_dir = Path(target_dir).resolve()
assemble_dir = Path(assemble_dir).resolve()
config = Path(config).resolve()

# Start

print('Packaging for Raspbian ARMv7...')
print(f'Running from "{os.getcwd()}"')
print(f'Using target directory "{target_dir}"')
print(f'Using assemble directory "{assemble_dir}"')

if os.path.isdir(assemble_dir): 
    print('Clearing assemble dir...')
    shutil.rmtree(assemble_dir)

print('Building server...')
result = subprocess.call(['./build_server_rpi.sh', 'deb_compiler'])
if result != 0:
    print('Failed to build server')
    sys.exit(1)

print('Building installing client dependencies...')
handle = subprocess.run(['yarn','install'], cwd=client_src)
if handle.returncode != 0:
    print(f'Failed to install client dependencies! Return code: {handle.returncode}')
    sys.exit(1)

print('Building client...')
handle = subprocess.run(['ng','build','--prod'], cwd=client_src)
if handle.returncode != 0:
    print(f'Failed to build client! Return code: {handle.returncode}')
    sys.exit(1)

print(f'Creating assembly tree "{assemble_dir}/client"')
os.makedirs(f'{assemble_dir}/client', exist_ok=True)

print(f'Copying client build into assembly directory: "{client_src}/dist" -> "{assemble_dir}/client"')
distutils.dir_util.copy_tree(f'{client_src}/dist', f'{assemble_dir}/client')

if target_bin.exists():
    target_bin_path = target_bin
else:
    target_bin_path = target_bin_debug

print(f'Copying server build into assembly directory: "{target_bin_path}" -> "{assemble_dir}"')
shutil.copy2(target_bin_path, assemble_dir)

print(f'Copying example config into assembly directory: "{config}" -> "{assemble_dir}/tanzbox.toml"')
shutil.copyfile(config, f'{assemble_dir}/tanzbox.toml')

print(f'Creating package "{package_name}.zip" in "{target_dir}"...')
with ZipFile(f'{target_dir}/{package_name}.zip', 'w', compression=ZIP_DEFLATED) as package:
    for root, dirs, files in os.walk(assemble_dir):
        root_path = Path(root)
        for filename in files:
            file_path = root_path / filename
            package.write(file_path, arcname=package_base_dir / file_path.relative_to(assemble_dir))
