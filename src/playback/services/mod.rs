use playback::streams::{AudioFormat, SampleFormat};
use playback::Metadata;
use sockets::protocol::SearchQuery;
use std::fmt;

pub mod local;
pub mod search;
pub mod soundcloud;
//pub mod spotify;

pub use self::local::LocalFiles;
pub use self::soundcloud::SoundCloud;
//pub use self::spotify::service::Spotify;

#[derive(Serialize, Deserialize, Clone, Eq, PartialEq, Hash, Debug, Copy)]
pub enum Service {
    Invalid,
    Local,
    Spotify,
    Youtube,
    SoundCloud,
}

impl Default for Service {
    fn default() -> Service {
        Service::Invalid
    }
}

impl fmt::Display for Service {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            &Service::Invalid => write!(f, "INVALID"),
            &Service::Local => write!(f, "Local"),
            &Service::SoundCloud => write!(f, "Soundcloud"),
            &Service::Spotify => write!(f, "Spotify"),
            &Service::Youtube => write!(f, "Youtube"),
        }
    }
}

pub trait CustomDecoder {
    fn load(&mut self, uri: &String);
}

pub trait AudioService {
    fn play(&mut self, format: &AudioFormat);
    fn pause(&self);
    fn resume(&self);
    fn stop(&self);
    fn shutdown(&mut self);
    fn is_ready(&self) -> bool;
    fn get_metadata(&self, uri: &String) -> Metadata;
    fn service_type(&self) -> Service;
    fn load(&mut self, uri: &String, format: &AudioFormat);
    fn prefered_sample_format(&self) -> Option<SampleFormat>;
    fn prefered_sample_rate(&self) -> Option<u32>;
    fn search(&self, query: &SearchQuery) -> Vec<String>;
}
