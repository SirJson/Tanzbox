use byteorder::{ByteOrder, NativeEndian};
use crossbeam_channel as message;
use prelude::*;
use std::io::prelude::*;
use std::io::BufReader;
use std::mem;
use std::process::{self, Command, Stdio};

#[derive(Debug, Eq, PartialEq, Clone)]
pub enum PipeMode {
    Null,
    Raw,
    Async,
    AsyncText,
    Inherit,
}

#[derive(Debug)]
pub struct Pipe {
    pub handle: process::Stdio,
    pub mode: PipeMode,
}

impl Pipe {
    pub fn raw() -> Pipe {
        Pipe {
            handle: Stdio::piped(),
            mode: PipeMode::Raw,
        }
    }
    pub fn null() -> Pipe {
        Pipe {
            handle: Stdio::null(),
            mode: PipeMode::Null,
        }
    }
    pub fn async() -> Pipe {
        Pipe {
            handle: Stdio::piped(),
            mode: PipeMode::Async,
        }
    }
    pub fn async_text() -> Pipe {
        Pipe {
            handle: Stdio::piped(),
            mode: PipeMode::AsyncText,
        }
    }
}

impl From<process::ChildStdout> for Pipe {
    fn from(other: process::ChildStdout) -> Self {
        Pipe {
            handle: Stdio::from(other),
            mode: PipeMode::Raw,
        }
    }
}

impl Default for Pipe {
    fn default() -> Self {
        Pipe {
            handle: Stdio::inherit(),
            mode: PipeMode::Inherit,
        }
    }
}

#[derive(Debug)]
pub struct RunCommand {
    pub program: String,
    pub args: Vec<String>,
}

#[derive(Debug)]
pub struct PipeConfig {
    pub stdin: Pipe,
    pub stdout: Pipe,
    pub stderr: Pipe,
}

impl Default for PipeConfig {
    fn default() -> Self {
        PipeConfig {
            stdin: Pipe::default(),
            stdout: Pipe::default(),
            stderr: Pipe::default(),
        }
    }
}

#[derive(Debug)]
pub enum StdioDataType {
    Unknown(usize),
    U8(usize),
    F32(usize),
    I16(usize),
    U16(usize),
}

#[derive(Debug)]
pub enum StdioData {
    U8(u8),
    F32(f32),
    I16(i16),
    U16(u16),
}

pub trait StdioType {}

impl StdioType for u8 {}

impl StdioType for f32 {}

impl StdioType for i16 {}

impl StdioType for u16 {}

fn get_type_info<T: StdioType>() -> StdioDataType {
    let u8_size = mem::size_of::<u8>();
    let f32_size = mem::size_of::<f32>();
    let i16_size = mem::size_of::<i16>();
    let u16_size = mem::size_of::<u16>();
    let own = mem::size_of::<T>();

    if own == u8_size {
        return StdioDataType::U8(u8_size);
    } else if own == f32_size {
        return StdioDataType::F32(f32_size);
    } else if own == i16_size {
        return StdioDataType::I16(i16_size);
    } else if own == u16_size {
        return StdioDataType::U16(u16_size);
    } else {
        return StdioDataType::Unknown(0);
    }
}

#[derive(Debug)]
pub struct Task {
    command: RunCommand,
    child: Option<process::Child>,
    out_tx: message::Sender<StdioData>,
    out_rx: message::Receiver<StdioData>,
    err_tx: message::Sender<StdioData>,
    err_rx: message::Receiver<StdioData>,
    txt_err_tx: message::Sender<String>,
    txt_err_rx: message::Receiver<String>,
}

impl Task {
    pub fn new(cmd: RunCommand) -> Task {
        let (out_tx, out_rx) = message::unbounded::<StdioData>();
        let (err_tx, err_rx) = message::unbounded::<StdioData>();
        let (txt_err_tx, txt_err_rx) = message::unbounded::<String>();

        Task {
            command: cmd,
            child: None,
            out_tx: out_tx,
            out_rx: out_rx,
            err_tx: err_tx,
            err_rx: err_rx,
            txt_err_tx: txt_err_tx,
            txt_err_rx: txt_err_rx,
        }
    }

    pub fn exec<T: StdioType>(&mut self, cfg: PipeConfig)
    where
        T: Send,
    {
        let mut process = Command::new(self.command.program.clone())
            .args(self.command.args.clone())
            .stdin(cfg.stdin.handle)
            .stdout(cfg.stdout.handle)
            .stderr(cfg.stderr.handle)
            .spawn()
            .unwrap();

        let out_cfg = cfg.stdout.mode;
        let err_cfg = cfg.stderr.mode;

        if out_cfg == PipeMode::Async {
            let mut stdout = process.stdout.take().unwrap();
            let tx = self.out_tx.clone();

            named_thread("proc_stdout_bytes", move || {
                let type_info = get_type_info::<T>();
                let size = match type_info {
                    StdioDataType::Unknown(val) => val,
                    StdioDataType::U8(val) => val,
                    StdioDataType::F32(val) => val,
                    StdioDataType::I16(val) => val,
                    StdioDataType::U16(val) => val,
                };
                let mut buffer = vec![0u8; size].into_boxed_slice();
                loop {
                    if let Err(error) = stdout.read_exact(&mut buffer) {
                        debug!("{:?}", error);
                        break;
                    } else {
                        match type_info {
                            StdioDataType::Unknown(_) => unimplemented!(),
                            StdioDataType::U8(_) => tx.send(StdioData::U8(buffer[0])),
                            StdioDataType::F32(_) => tx.send(StdioData::F32(NativeEndian::read_f32(&buffer))),
                            StdioDataType::I16(_) => tx.send(StdioData::I16(NativeEndian::read_i16(&buffer))),
                            StdioDataType::U16(_) => tx.send(StdioData::U16(NativeEndian::read_u16(&buffer))),
                        };
                    }
                }
            });
        }

        if err_cfg == PipeMode::Async || err_cfg == PipeMode::AsyncText {
            let stderr = process.stderr.take().unwrap();
            let tx = self.err_tx.clone();
            let txt_tx = self.txt_err_tx.clone();
            let reader = BufReader::new(stderr);

            if err_cfg == PipeMode::Async {
                named_thread("proc_stderr_bytes", move || {
                    let type_info = get_type_info::<T>();
                    let size = match type_info {
                        StdioDataType::Unknown(val) => val,
                        StdioDataType::U8(val) => val,
                        StdioDataType::F32(val) => val,
                        StdioDataType::I16(val) => val,
                        StdioDataType::U16(val) => val,
                    };
                    let mut buffer = vec![0u8; size].into_boxed_slice();
                    let mut buffer_pos = 0;
                    let mut itter = reader.bytes();
                    loop {
                        if let Some(byte) = itter.next() {
                            buffer[buffer_pos] = byte.unwrap_or_default();
                            buffer_pos = buffer_pos + 1;
                            if buffer_pos >= size {
                                match type_info {
                                    StdioDataType::Unknown(_) => unimplemented!(),
                                    StdioDataType::U8(_) => tx.send(StdioData::U8(buffer[0])),
                                    StdioDataType::F32(_) => tx.send(StdioData::F32(NativeEndian::read_f32(&buffer))),
                                    StdioDataType::I16(_) => tx.send(StdioData::I16(NativeEndian::read_i16(&buffer))),
                                    StdioDataType::U16(_) => tx.send(StdioData::U16(NativeEndian::read_u16(&buffer))),
                                };
                                buffer_pos = 0;
                            }
                        }
                    }
                });
            } else {
                named_thread("proc_stderr_text", move || {
                    for line in reader.lines() {
                        txt_tx.send(line.unwrap());
                    }
                });
            }
        }

        self.child = Some(process);
    }

    pub fn alive(&mut self) -> bool {
        let process = self.child.as_mut().unwrap();

        match process.try_wait() {
            Ok(Some(_)) => false,
            Ok(None) => true,
            Err(e) => {
                println!("error attempting to wait: {}", e);
                false
            }
        }
    }

    pub fn take_stdout(&mut self) -> Option<process::ChildStdout> {
        self.child.as_mut().unwrap().stdout.take()
    }

    pub fn stdout(&self) -> StdoutIterator {
        StdoutIterator { rx: self.out_rx.clone() }
    }

    pub fn stderr_text(&self) -> StderrTextIterator {
        StderrTextIterator { rx: self.txt_err_rx.clone() }
    }
}

pub struct StdoutIterator {
    rx: message::Receiver<StdioData>,
}

pub struct StderrTextIterator {
    rx: message::Receiver<String>,
}

impl Iterator for StdoutIterator {
    type Item = StdioData;
    fn next(&mut self) -> Option<StdioData> {
        self.rx.try_recv()
    }
}

impl Iterator for StderrTextIterator {
    type Item = String;
    fn next(&mut self) -> Option<String> {
        self.rx.try_recv()
    }
}
