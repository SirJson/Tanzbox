
export class Command {
    id: number;
    options: {}
}

export enum CommandIdentifier {
    Invalid = 0x0000000,
    // Client Commands
    LoginRequest = 0x0000001,
    RegisterAck,
    SpotifyToken,
    SpotifyPlaySong,
    PlaySong,

    // Server Commands
    PlayerLogin = 0x7ffffffe,
    RegisterUser,
    SpotifyPlayerReady
}

/*

pub const INVALID: i64 = 0x00000000;

// Client Commands
pub const LOGIN_REQUEST: i64 = 0x0000001;
pub const REGISTER_ACK: i64 = 0x00000002;
pub const SPOTIFY_TOKEN: i64 = 0x00000003;
pub const SPOTIFY_PLAY_SONG: i64 = 0x00000004;
pub const PLAY_SONG: i64 = 0x00000005;

// Server Commands
pub const LOGIN: i64 = 0x7ffffffe;
pub const SPOTIFY_AUTH: i64 = 0x7fffffff;
pub const REGISTER_USER: i64 = 0x80000000;
pub const SPOTIFY_PLAYER_READY: i64 = 0x80000001;

*/